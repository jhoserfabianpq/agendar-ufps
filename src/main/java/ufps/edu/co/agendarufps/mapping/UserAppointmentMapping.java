package ufps.edu.co.agendarufps.mapping;

import org.modelmapper.ModelMapper;
import ufps.edu.co.agendarufps.dtos.UserAppointmentDto;
import ufps.edu.co.agendarufps.entity.UserAppointment;

import java.util.List;
import java.util.stream.Collectors;

public class UserAppointmentMapping {
    private static final ModelMapper modelMapper = new ModelMapper();
    public static UserAppointmentDto userAppointmentEntityToUserAppointmentDto(UserAppointment userAppointment) {
        return modelMapper.map(userAppointment, UserAppointmentDto.class);
    }

    public static UserAppointment userAppointmentDtoToUserAppointmentEntity(UserAppointmentDto userAppointmentDto) {
        return modelMapper.map(userAppointmentDto, UserAppointment.class);
    }

    public static List<UserAppointmentDto> listUserAppointmentEntityToUserAppointmentDto(List<UserAppointment> userAppointments) {
        return userAppointments.stream()
                .map(UserAppointmentMapping::userAppointmentEntityToUserAppointmentDto)
                .collect(Collectors.toList());
    }

    public static List<UserAppointment> listUserAppointmentDtoToUserAppointmentEntity(List<UserAppointmentDto> userAppointmentDtos) {
        return userAppointmentDtos.stream()
                .map(UserAppointmentMapping::userAppointmentDtoToUserAppointmentEntity)
                .collect(Collectors.toList());
    }
}
