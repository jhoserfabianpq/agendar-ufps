package ufps.edu.co.agendarufps.mapping;

import org.modelmapper.ModelMapper;
import ufps.edu.co.agendarufps.dtos.AppointmentDto;
import ufps.edu.co.agendarufps.entity.Appointment;

import java.util.List;
import java.util.stream.Collectors;

public class AppointmentMapping {

    private static final ModelMapper modelMapper = new ModelMapper();

    public static AppointmentDto appointmentEntityToAppointmentDto(Appointment appointmentEntity) {
        return modelMapper.map(appointmentEntity, AppointmentDto.class);
    }

    public static Appointment appointmentDtoToAppointmentEntity(AppointmentDto appointmentDto) {
        return modelMapper.map(appointmentDto, Appointment.class);
    }

    public static List<AppointmentDto> listAppointmentEntityToAppointmentDto(List<Appointment> appointmentEntities) {
        return appointmentEntities.stream()
                .map(AppointmentMapping::appointmentEntityToAppointmentDto)
                .collect(Collectors.toList());
    }
    public static List<Appointment> listAppointmentDtoToAppointmentEntity(List<AppointmentDto> appointmentDto) {
        return appointmentDto.stream()
                .map(AppointmentMapping::appointmentDtoToAppointmentEntity)
                .collect(Collectors.toList());
    }
}