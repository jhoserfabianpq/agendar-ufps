package ufps.edu.co.agendarufps.mapping;

import org.modelmapper.ModelMapper;
import ufps.edu.co.agendarufps.dtos.IntervalDto;
import ufps.edu.co.agendarufps.entity.Interval;

import java.util.List;
import java.util.stream.Collectors;

public class IntervalMapping {
    private static final ModelMapper modelMapper = new ModelMapper();
    public static IntervalDto intervalEntityToIntervalDto(Interval interval) {
        return modelMapper.map(interval, IntervalDto.class);
    }

    public static Interval intervalDtoToIntervalDEntity(IntervalDto interval) {
        return modelMapper.map(interval, Interval.class);
    }

    public static List<IntervalDto> listIntervalEntityToIntervalDto(List<Interval> intervals) {
        return intervals.stream()
                .map(IntervalMapping::intervalEntityToIntervalDto)
                .collect(Collectors.toList());
    }

    public static List<Interval> listIntervalDtoToIntervalEntity(List<IntervalDto> intervalDtos) {
        return intervalDtos.stream()
                .map(IntervalMapping::intervalDtoToIntervalDEntity)
                .collect(Collectors.toList());
    }
}
