package ufps.edu.co.agendarufps.mapping;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import ufps.edu.co.agendarufps.dtos.EventDto;
import ufps.edu.co.agendarufps.dtos.UserDto;
import ufps.edu.co.agendarufps.dtos.UserInfo;
import ufps.edu.co.agendarufps.entity.EventEntity;
import ufps.edu.co.agendarufps.entity.User;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
public class UserMapping {

    private static final ModelMapper modelMapper = new ModelMapper();
    public User UserInfoToUser(UserInfo userInfo) {
        return User.builder()
                .email(userInfo.getEmail())
                .firstName(userInfo.getGiven_name())
                .lastName(userInfo.getFamily_name())
                .pictureUrl(userInfo.getPicture())
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
    }
    public User userPrincipalToUser(OAuth2AuthenticatedPrincipal user){
        Map<String,Object> attributes = user.getAttributes();
        return User.builder()
                .email(attributes.get("email").toString())
                .firstName(attributes.get("first_name").toString())
                .lastName(attributes.get("last_name").toString())
                .pictureUrl(attributes.get("picture_url").toString())
                .createdAt((LocalDateTime) attributes.get("created_at"))
                .updatedAt((LocalDateTime) attributes.get("updated_at"))
                .build();
    }

    public static UserDto userEntityToUserDto(User user){
            return modelMapper.map(user, UserDto.class);
    }
    public static User userDtoToUserEntity(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }

    private static List<EventEntity> mapEventsEntities(List<EventDto> eventDtos) {
        return Optional.ofNullable(eventDtos)
                .map(dtos -> dtos.stream()
                        .map(EventMapping::eventDtoToEventEntity)
                        .peek(eventEntity -> eventEntity.setUser(null)) // Para evitar referencia circular
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }
    private static List<EventDto> mapEventsDtos(List<EventEntity> eventEntities) {
        return Optional.ofNullable(eventEntities)
                .map(entity-> entity.stream()
                        .map(EventMapping::eventEntityToEventDto)
                        .peek(eventEntity -> eventEntity.setUser(null)) // Para evitar referencia circular
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    public static List<UserDto> listUserEntityToUserDto(List<User> users) {
        return users.stream()
                .map(UserMapping::userEntityToUserDto)
                .collect(Collectors.toList());
    }

    public static List<User> listUserDtoToUserEntity(List<UserDto> userDtos) {
        return userDtos.stream()
                .map(UserMapping::userDtoToUserEntity)
                .collect(Collectors.toList());
    }


}
