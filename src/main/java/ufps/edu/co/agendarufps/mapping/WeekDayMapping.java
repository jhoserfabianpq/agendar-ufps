package ufps.edu.co.agendarufps.mapping;

import org.modelmapper.ModelMapper;
import ufps.edu.co.agendarufps.dtos.WeekDayDto;
import ufps.edu.co.agendarufps.entity.WeekDay;

import java.util.List;
import java.util.stream.Collectors;

public class WeekDayMapping {
    private static final ModelMapper modelMapper = new ModelMapper();
    public static WeekDayDto weekDayEntityToWeekDayDto(WeekDay weekDay){
        return modelMapper.map(weekDay, WeekDayDto.class);
    }
    public static WeekDay weekDayDtoToWeekDayEntity(WeekDayDto weekDay){
        return modelMapper.map(weekDay, WeekDay.class);
    }

    public static List<WeekDayDto> listWeekDayEntityToWeekDayDto(List<WeekDay> intervals) {
        return intervals.stream()
                .map(WeekDayMapping::weekDayEntityToWeekDayDto)
                .collect(Collectors.toList());
    }

    public static List<WeekDay> listWeekDayDtoToWeekDayEntity(List<WeekDayDto> intervalDtos) {
        return intervalDtos.stream()
                .map(WeekDayMapping::weekDayDtoToWeekDayEntity)
                .collect(Collectors.toList());
    }
}
