package ufps.edu.co.agendarufps.mapping;

import org.modelmapper.ModelMapper;
import ufps.edu.co.agendarufps.dtos.TopicDto;
import ufps.edu.co.agendarufps.entity.Topic;

import java.util.List;
import java.util.stream.Collectors;

public class TopicMapping {
    private static final ModelMapper modelMapper = new ModelMapper();
    public static TopicDto topicEntityToTopicDto(Topic topic) {
        return modelMapper.map(topic, TopicDto.class);
    }

    public static Topic topicDtoToTopicEntity(TopicDto topicDto) {
        return modelMapper.map(topicDto, Topic.class);
    }

    public static List<TopicDto> listTopicEntityToTopicDto(List<Topic> topics) {
        return topics.stream()
                .map(TopicMapping::topicEntityToTopicDto)
                .collect(Collectors.toList());
    }

    public static List<Topic> listTopicDtoToTopicEntity(List<TopicDto> topicDtos) {
        return topicDtos.stream()
                .map(TopicMapping::topicDtoToTopicEntity)
                .collect(Collectors.toList());
    }
}
