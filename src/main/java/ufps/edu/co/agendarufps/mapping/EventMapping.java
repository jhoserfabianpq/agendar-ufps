package ufps.edu.co.agendarufps.mapping;

import org.modelmapper.ModelMapper;
import ufps.edu.co.agendarufps.dtos.AvailabilityDto;
import ufps.edu.co.agendarufps.dtos.EventDto;
import ufps.edu.co.agendarufps.entity.Availability;
import ufps.edu.co.agendarufps.entity.EventEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class EventMapping {

    private static final ModelMapper modelMapper = new ModelMapper();
    public static EventDto eventEntityToEventDto(EventEntity eventEntity) {
        return modelMapper.map(eventEntity, EventDto.class);
    }

    public static EventEntity eventDtoToEventEntity(EventDto eventEntity) {
        return modelMapper.map(eventEntity, EventEntity.class);
    }

    private static List<Availability> mapAvailabilities(List<AvailabilityDto> availabilityDtos) {
        return Optional.ofNullable(availabilityDtos)
                .map(dtos -> dtos.stream()
                        .map(AvailabilityMapping::availabilityDtoToAvailabilityEntity)
                        .peek(availabilityEntity -> availabilityEntity.setEventEntity(null)) // Para evitar referencia circular
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }
    public static List<EventDto> listEventEntityToEventDto(List<EventEntity> events) {
        List<EventDto> eventDtos = new ArrayList<>();

        for (EventEntity eventEntity : events) {
            EventDto eventDto = EventMapping.eventEntityToEventDto(eventEntity);
            eventDtos.add(eventDto);
        }

        return eventDtos;
    }

    public static List<EventEntity> listEventDtoToEventEntity(List<EventDto> eventDtos) {
        List<EventEntity> eventEntities = new ArrayList<>();
        for (EventDto eventDto : eventDtos) {
            EventEntity eventEntity = EventMapping.eventDtoToEventEntity(eventDto);
            eventEntities.add(eventEntity);
        }
        return eventEntities;
    }
}
