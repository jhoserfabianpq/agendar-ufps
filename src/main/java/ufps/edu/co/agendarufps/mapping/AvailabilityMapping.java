package ufps.edu.co.agendarufps.mapping;

import org.modelmapper.ModelMapper;
import ufps.edu.co.agendarufps.dtos.AvailabilityDto;
import ufps.edu.co.agendarufps.entity.Availability;

import java.util.List;
import java.util.stream.Collectors;

public class AvailabilityMapping {
    private static final ModelMapper modelMapper = new ModelMapper();
    public static AvailabilityDto availabilityEntityToAvailabilityDto(Availability availability) {
        return modelMapper.map(availability, AvailabilityDto.class);
    }

    public static Availability availabilityDtoToAvailabilityEntity(AvailabilityDto availabilityDto) {
        return modelMapper.map(availabilityDto, Availability.class);
    }

    public static List<AvailabilityDto> listAvailabilityDtoToAvailabilityEntity(List<Availability> availabilities) {
        return availabilities.stream()
                .map(AvailabilityMapping::availabilityEntityToAvailabilityDto)
                .collect(Collectors.toList());
    }

    public static List<Availability> listAvailabilityEntityToAvailabilityDto(List<AvailabilityDto> availabilityDtos) {
        return availabilityDtos.stream()
                .map(AvailabilityMapping::availabilityDtoToAvailabilityEntity)
                .collect(Collectors.toList());
    }
}
