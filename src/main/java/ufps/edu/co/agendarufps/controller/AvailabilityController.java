package ufps.edu.co.agendarufps.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.*;
import ufps.edu.co.agendarufps.dtos.AvailabilityDto;
import ufps.edu.co.agendarufps.entity.Availability;
import ufps.edu.co.agendarufps.mapping.AvailabilityMapping;
import ufps.edu.co.agendarufps.service.exception.AvailabilityException;
import ufps.edu.co.agendarufps.service.impl.AvailabilityServiceImpl;
import ufps.edu.co.agendarufps.utils.MessageConstants;

import java.util.List;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
public class AvailabilityController {

    private final AvailabilityServiceImpl availabilityServiceImpl;
    private final ObjectMapper objectMapper;
    @PostMapping(value = "/availability/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createAvailability(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user,
            @RequestBody AvailabilityDto availability) throws JsonProcessingException {
        try {
            String email = user.getAttribute("email");
            AvailabilityDto newAvailability = availabilityServiceImpl.saveAvailability(availability, email);
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(newAvailability));
        }catch(AvailabilityException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(e.getMessage()));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(MessageConstants.EMPTY_FIELDS));
        }
    }
    @GetMapping(value = "/availability/{idAvailability}/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAvailability(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user,
            @PathVariable("idAvailability") Long idAvailability) throws JsonProcessingException {
        try {
            Availability newAvailability = availabilityServiceImpl.getAvailability(idAvailability);
            if (newAvailability != null && Objects.equals(user.getAttribute("email"), newAvailability.getCreatedBy().getEmail())) {
                AvailabilityDto availabilityDto = AvailabilityMapping.availabilityEntityToAvailabilityDto(newAvailability);
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(availabilityDto));
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.AVAILABILITY_NOT_FOUND));
            }
        }catch (AvailabilityException.AvailabilityNotFoundException e){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(e.getMessage()));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.AVAILABILITY_ERROR));
        }
    }

    @GetMapping(value = "/availability/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllAvailability(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user) throws JsonProcessingException {
        try{
            List<AvailabilityDto> newAvailability = availabilityServiceImpl.getAllAvailability(user.getAttribute("email"));
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(newAvailability));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.AVAILABILITY_NOT_FOUND));
        }
    }

    @PutMapping(value = "/availability/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateAvailability(@RequestBody AvailabilityDto availability) throws JsonProcessingException {
        try{
            AvailabilityDto newAvailability = availabilityServiceImpl.updateAvailability(availability);
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(newAvailability));
        }catch (AvailabilityException.AvailabilityNotFoundException | AvailabilityException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(e.getMessage()));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(MessageConstants.AVAILABILITY_ERROR));
        }
    }

    @DeleteMapping(value="availability/{idAvailability}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteAvailability(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user,
                                                     @PathVariable Long idAvailability) throws JsonProcessingException {
            try {
                Availability availability = availabilityServiceImpl.getAvailability(idAvailability);
                if (availability != null && availability.getCreatedBy().getEmail().equals(user.getAttribute("email"))) {
                    availabilityServiceImpl.deleteAvailability(availability.getId());
                    return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(MessageConstants.AVAILABILITY_DELETE));
                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.AVAILABILITY_NOT_FOUND));
                }
            }catch (AvailabilityException.AvailabilityNotFoundException | AvailabilityException.AvailabilityInUseException e){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(e.getMessage()));
            } catch (Exception e){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(MessageConstants.AVAILABILITY_ERROR));
            }
    }
}
