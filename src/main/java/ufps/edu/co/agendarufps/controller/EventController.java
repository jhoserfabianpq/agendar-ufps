package ufps.edu.co.agendarufps.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.*;
import ufps.edu.co.agendarufps.dtos.CalendarSlotsDto;
import ufps.edu.co.agendarufps.dtos.EventDto;
import ufps.edu.co.agendarufps.entity.EventEntity;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.mapping.EventMapping;
import ufps.edu.co.agendarufps.mapping.UserMapping;
import ufps.edu.co.agendarufps.service.exception.EventException;
import ufps.edu.co.agendarufps.service.impl.EventServiceImpl;
import ufps.edu.co.agendarufps.service.impl.SlotsIntervalsImp;
import ufps.edu.co.agendarufps.service.impl.UserServiceImpl;
import ufps.edu.co.agendarufps.utils.MessageConstants;
import ufps.edu.co.agendarufps.utils.Utilities;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;


@RestController
@RequiredArgsConstructor
public class EventController {
    private final Utilities utilities;
    private final UserServiceImpl userServiceImpl;
    private final EventServiceImpl eventServiceImpl;
    private final ObjectMapper objectMapper;
    private final SlotsIntervalsImp slotsIntervalService;


    @PostMapping(value = "event/{userEmail}/", produces = "application/json")
    public ResponseEntity<String> createEvent(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user,
                                              @PathVariable("userEmail") String userEmail,
                                              @RequestBody EventDto eventEntity) throws JsonProcessingException {
        if(userEmail.isBlank() || !userEmail.equals(utilities.getEmailAddressWithoutDomain(Objects.requireNonNull(user.getAttribute("email"))))){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(MessageConstants.USER_NOT_FOUND);
        }
        try {
            User newUser = userServiceImpl.findUserByEmail(user.getAttribute("email"));
            eventEntity.setUser(UserMapping.userEntityToUserDto(newUser));
            EventEntity saveEventEntity = eventServiceImpl.saveEvent(EventMapping.eventDtoToEventEntity(eventEntity));
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(EventMapping.eventEntityToEventDto(saveEventEntity)));
        }catch (EventException.EventAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(e.getMessage()));
        }catch (EventException.EventNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(e.getMessage()));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.EVENT_ERROR));
        }
    }
    @GetMapping(value = "public/event/{userEmail}/{eventName}/", produces = "application/json")
    public ResponseEntity<String> getPublicEvent(@PathVariable("userEmail") String userEmail, @PathVariable("eventName") String eventName) throws JsonProcessingException {
        if(userEmail.isBlank() || eventName.isBlank()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(MessageConstants.EVENT_NOT_FOUND + " or " + MessageConstants.USER_NOT_FOUND );
        }
        try {
            EventEntity eventEntity = eventServiceImpl.getEventByNameAndEmail(eventName, userEmail);
            EventDto evetEventDto = EventMapping.eventEntityToEventDto(eventEntity);
            if (evetEventDto != null && evetEventDto.getState() == 1) {
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(evetEventDto));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(MessageConstants.EVENT_NOT_FOUND));
            }
        }catch (EventException.EventNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(e.getMessage()));
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.EVENT_NOT_FOUND));
        }
    }

    @GetMapping(value = "/event/{userEmail}/{eventName}/", produces = "application/json")
    public ResponseEntity<String> getEvent(@PathVariable("userEmail") String userEmail, @PathVariable("eventName") String eventName) throws JsonProcessingException {
        if(userEmail.isBlank() || eventName.isBlank()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(MessageConstants.EVENT_NOT_FOUND + " or " + MessageConstants.USER_NOT_FOUND );
        }
        try {
            EventEntity eventEntity = eventServiceImpl.getEventByNameAndEmail(eventName, userEmail);
            EventDto evetEventDto = EventMapping.eventEntityToEventDto(eventEntity);
            if (evetEventDto != null) {
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(evetEventDto));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(MessageConstants.EVENT_NOT_FOUND));
            }
        }catch (EventException.EventNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(e.getMessage()));
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.EVENT_NOT_FOUND));
        }
    }

    @GetMapping(value = "public/event/{userEmail}/{eventName}/topics", produces = "application/json")
    public ResponseEntity<String> getTopicsEvent(@PathVariable("userEmail") String userEmail,
                                                 @PathVariable("eventName") String eventName) throws JsonProcessingException {
        if(userEmail.isBlank() || eventName.isBlank()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(MessageConstants.EVENT_NOT_FOUND + " or " + MessageConstants.USER_NOT_FOUND );
        }
        try {
            EventEntity eventEntity = eventServiceImpl.getEventByNameAndEmail(eventName, userEmail);
            EventDto evetEventDto = EventMapping.eventEntityToEventDto(eventEntity);
            if (evetEventDto != null && evetEventDto.getState() == 1) {
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(evetEventDto.getTopics()));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(MessageConstants.TOPIC_NOT_FOUND));
            }
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.TOPIC_NOT_FOUND));
        }
    }

    @DeleteMapping(value = "event/{userEmail}/{eventName}/", produces = "application/json")
    public ResponseEntity<String> deleteEvent(@PathVariable("userEmail") String userEmail,
                                              @PathVariable("eventName") String eventName) throws JsonProcessingException {
        if(userEmail.isBlank() || eventName.isBlank()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(MessageConstants.EVENT_NOT_FOUND + " or " + MessageConstants.USER_NOT_FOUND );
        }
        try {
            EventEntity eventEntity = eventServiceImpl.getEventByNameAndEmail(eventName, userEmail);
            EventDto evetEventDto = EventMapping.eventEntityToEventDto(eventEntity);
            if (evetEventDto != null) {
                eventServiceImpl.deleteEvent(eventEntity.getId());
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(MessageConstants.EVENT_DELETE));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(MessageConstants.EVENT_NOT_FOUND));
            }
        }
        catch (EventException.EventNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(e.getMessage()));
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(
                    MessageConstants.EVENT_ERROR));
        }
    }

    @PutMapping(value = "event/{userEmail}/{eventName}/", produces = "application/json")
    public ResponseEntity<String> updateEvent(@PathVariable("userEmail") String userEmail,
                                              @PathVariable("eventName") String eventName,
                                              @RequestBody EventDto eventDto) throws JsonProcessingException {
        if(userEmail.isBlank() || eventName.isBlank()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(
                    MessageConstants.EVENT_NAME_IS_REQUIRED + " or " + MessageConstants.USER_NOT_FOUND));
        }
        try {
            //EventEntity eventEntity = eventServiceImpl.getEventByNameAndEmail(eventName, userEmail);
            if (eventDto != null) {
                EventEntity event = eventServiceImpl.updateEvent(EventMapping.eventDtoToEventEntity(eventDto));
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(EventMapping.eventEntityToEventDto(event)));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(MessageConstants.EVENT_NOT_FOUND));
            }
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(
                    MessageConstants.EVENT_NOT_FOUND + e.getMessage()));
        }
    }

    @GetMapping(value = "public/event/{userEmail}/{eventName}", produces = "application/json")
    public ResponseEntity<String> getCalendarSlotsAvailability(@PathVariable("userEmail") String userEmail,
                                                         @PathVariable("eventName") String eventName,
                                                          @RequestParam (name = "date") LocalDate date) throws JsonProcessingException {
        if(userEmail.isBlank() || eventName.isBlank()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(
                    MessageConstants.EVENT_NOT_FOUND + " or " + MessageConstants.USER_NOT_FOUND));
        }
        try {
            EventEntity eventEntity = eventServiceImpl.getEventByNameAndEmail(eventName, userEmail);
            EventDto evetEventDto = EventMapping.eventEntityToEventDto(eventEntity);
            if (evetEventDto != null) {
                CalendarSlotsDto calendarSlotsDto = slotsIntervalService.getIntervalAvailability(eventEntity.getId(), date);
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(calendarSlotsDto));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(MessageConstants.EVENT_NOT_FOUND));
            }
        }catch (EventException.EventNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(e.getMessage()));
        } catch (EventException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.EVENT_ERROR));
        }
    }

    @GetMapping(value = "event/{userEmail}/", produces = "application/json")
    public ResponseEntity<String> getAllEvent(@PathVariable("userEmail") String userEmail) throws JsonProcessingException {
        User user = userServiceImpl.findByUsernameWithoutDomain(userEmail);
        if(user == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(MessageConstants.USER_NOT_FOUND));
        }else{
            List<EventEntity> allEventEntities = eventServiceImpl.getAllEventByUserEmail(user.getEmail());
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(EventMapping.listEventEntityToEventDto(allEventEntities)));
        }
    }
}
