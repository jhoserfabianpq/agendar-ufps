package ufps.edu.co.agendarufps.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ufps.edu.co.agendarufps.dtos.AttendeesAppointmentDto;
import ufps.edu.co.agendarufps.entity.Historic;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.service.impl.AppointmentServiceImpl;
import ufps.edu.co.agendarufps.service.impl.HistoricServiceImpl;
import ufps.edu.co.agendarufps.service.impl.ReportServiceImpl;
import ufps.edu.co.agendarufps.service.impl.UserServiceImpl;
import ufps.edu.co.agendarufps.utils.MessageConstants;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class ReportController {
    private final HistoricServiceImpl historicService;
    private final UserServiceImpl userService;
    private final ReportServiceImpl reportService;
    private final AppointmentServiceImpl appointmentService;
    private final ObjectMapper objectMapper;
/*
    @GetMapping(value ="/report-email/", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> getReportByEmail(@RequestParam String email){
        try {
            User emailOrganizer = userService.findByUsernameWithoutDomain(email);
            if(emailOrganizer == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(MessageConstants.USER_NOT_FOUND.getBytes());
            List<Historic> historicList = historicService.getHistoricByEmailOrganizer(emailOrganizer.getEmail());
            byte[] excelReport = reportService.generateExcelReport(historicList);
            if (excelReport == null) return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(MessageConstants.REPORT_ERROR.getBytes());
            return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.valueOf(MediaType.APPLICATION_OCTET_STREAM_VALUE)).body(excelReport);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(MessageConstants.REPORT_ERROR.getBytes());
        }
    }
*/
    @GetMapping(value ="/report-period/")
    public ResponseEntity<byte[]> getReportByPeriod(@RequestParam String email, @RequestParam String period){
        try {
            List<Historic> historicList = historicService.getHistoricByEmailOrganizerAndPeriod(email, period);
            byte[] excelReport = reportService.generateExcelReport(historicList);
            if (excelReport == null) return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(MessageConstants.REPORT_ERROR.getBytes());
            return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.valueOf(MediaType.APPLICATION_OCTET_STREAM_VALUE)).body(excelReport);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(MessageConstants.REPORT_ERROR.getBytes());
        }
    }
/*
    @GetMapping(value ="/report")
    public ResponseEntity<byte[]> getReportByDate(@RequestParam LocalDate start, @RequestParam LocalDate end){
        try {
            List<Historic> historicList = historicService.getHistoricBetweenDate(start, end);
            byte[] excelReport = reportService.generateExcelReport(historicList);
            if (excelReport == null) return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(MessageConstants.REPORT_ERROR.getBytes());
            return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.valueOf(MediaType.APPLICATION_OCTET_STREAM_VALUE)).body(excelReport);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(MessageConstants.REPORT_ERROR.getBytes());
        }
    }
*/
    @GetMapping(value ="report-general/", produces = "application/json")
    public ResponseEntity<String> getGeneralReport(@RequestParam String email, @RequestParam String period){
        try {
            User emailOrganizer = userService.findUserByEmail(email);
            if(emailOrganizer == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(MessageConstants.USER_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(
                    reportService.getGeneralReport(emailOrganizer.getEmail(), period)));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(MessageConstants.REPORT_ERROR + e.getMessage());
        }
    }


    @GetMapping(value = "/report-appointment/{period}", produces = "application/json")
    public ResponseEntity<String> getAllMyAppointmentsXPeriod(
            @AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user,
            @PathVariable String period) throws JsonProcessingException {
        try{
            User u = userService.findUserByEmail(user.getAttribute("email"));
            List<AttendeesAppointmentDto> listAppointment = appointmentService.getAllAppointmentByPeriod(
                    u, period);
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(listAppointment));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }
    }

}
