package ufps.edu.co.agendarufps.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ufps.edu.co.agendarufps.dtos.TokenDto;
import ufps.edu.co.agendarufps.dtos.UrlDto;
import ufps.edu.co.agendarufps.service.impl.AuthServiceImpl;
import ufps.edu.co.agendarufps.utils.MessageConstants;

import java.util.Map;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private final AuthServiceImpl authServiceImpl;


    @GetMapping("/auth/url")
    public ResponseEntity<UrlDto> auth() {
        UrlDto url = authServiceImpl.getUrlAuthorization();
        if (url == null) {
            return ResponseEntity.internalServerError().body(new UrlDto("Error: Invalid authorization"));
        }
        return ResponseEntity.ok(url);
    }

    @GetMapping("/auth/callback")
    public ResponseEntity<TokenDto> callback(@RequestParam("code") String code) {
        if(code.isBlank()){
            ResponseEntity.BodyBuilder response = ResponseEntity.status(HttpStatus.UNAUTHORIZED);
            response.body(MessageConstants.UNAUTHORIZED + " " + "Code is required.");
            return response.build();
        }
        String token;
        try {
            token = authServiceImpl.generateToken(code);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(new TokenDto(token));
    }

    @GetMapping("/user")
    public ResponseEntity<Map<String, Object>> getUser(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal name) {
        return ResponseEntity.ok(name.getAttributes());
    }

    /*@GetMapping("/auth/logout")
    public ResponseEntity<String> logout() {
        authServiceImpl.logout();
        return ResponseEntity.ok("Logout successful");
    }*/
}
