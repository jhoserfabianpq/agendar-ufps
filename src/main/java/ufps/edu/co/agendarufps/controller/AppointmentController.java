package ufps.edu.co.agendarufps.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.*;
import ufps.edu.co.agendarufps.dtos.AppointmentDto;
import ufps.edu.co.agendarufps.dtos.AttendeesAppointmentDto;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.service.exception.AppointmentException;
import ufps.edu.co.agendarufps.service.impl.AppointmentServiceImpl;
import ufps.edu.co.agendarufps.service.impl.UserServiceImpl;
import ufps.edu.co.agendarufps.utils.MessageConstants;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AppointmentController {

    private final UserServiceImpl userServiceImpl;
    private final AppointmentServiceImpl appointmentServiceImpl;
    private final ObjectMapper objectMapper;
    @PostMapping(value = "/appointment/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createAppointment(@RequestBody AppointmentDto appointment) throws JsonProcessingException {
        try {
            AppointmentDto saveAppointment = appointmentServiceImpl.saveAppointment(appointment);
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(saveAppointment));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }
    }

    @GetMapping(value = "/appointment/upcoming", produces = "application/json")
    public ResponseEntity<String> getAllAppointmentsUpcoming(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user) throws JsonProcessingException {
        try{
            User u = userServiceImpl.findUserByEmail(user.getAttribute("email"));
            List<AppointmentDto> listAppointment = appointmentServiceImpl.getAllAppointmentUpcoming(u);
            if (listAppointment.isEmpty()){
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(MessageConstants.EMPTY_APPOINTMENT_UPCOMING));
            }
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(listAppointment));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }

    }
    @GetMapping(value = "/appointment/my/", produces = "application/json")
    public ResponseEntity<String> getAllMyAppointments(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user) throws JsonProcessingException {
        try{
            User u = userServiceImpl.findUserByEmail(user.getAttribute("email"));
            List<AttendeesAppointmentDto> listAppointment = appointmentServiceImpl.getAllMyAppointmentCreator(u);
            if (listAppointment.isEmpty()){
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(MessageConstants.EMPTY_APPOINTMENT));
            }
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(listAppointment));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }

    }

    @GetMapping(value = "/appointment/", produces = "application/json")
    public ResponseEntity<String> getAllAppointments(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user) throws JsonProcessingException {
        try{
            User u = userServiceImpl.findUserByEmail(user.getAttribute("email"));
            List<AppointmentDto> listAppointment = appointmentServiceImpl.getAllAppointment(u);
            if (listAppointment.isEmpty()){
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(MessageConstants.EMPTY_APPOINTMENT));
            }
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(listAppointment));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }

    }

    @GetMapping(value = "/appointment/invited/", produces = "application/json")
    public ResponseEntity<String> getAllAppointmentsInvited(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user) throws JsonProcessingException {
        try{
            User u = userServiceImpl.findUserByEmail(user.getAttribute("email"));
            List<AppointmentDto> listAppointment = appointmentServiceImpl.getAllAppointmentInvite(u);
            if (listAppointment.isEmpty()){
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(MessageConstants.EMPTY_APPOINTMENT));
            }
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(listAppointment));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }

    }

    @PutMapping(value = "appointment/{appointmentId}/user/{userId}/attendance", produces = "application/json")
    public ResponseEntity<String> markAttendance(@PathVariable int appointmentId,
                                               @PathVariable int userId,
                                               @RequestParam boolean attended) throws JsonProcessingException {
        try {
            appointmentServiceImpl.markAttendance(appointmentId, userId, attended);
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(MessageConstants.APPOINTMENT_MARK_ATTENDED));
        } catch (AppointmentException.AppointmentNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(e.getMessage()));
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }
    }

    @GetMapping (value = "appointment/{appointmentId}/", produces = "application/json")
    public ResponseEntity<String> getAppointment(@PathVariable int appointmentId,
                                                 @AuthenticationPrincipal OAuth2AuthenticatedPrincipal user) throws JsonProcessingException {
        try {
            String email = user.getAttribute("email");
            AttendeesAppointmentDto userAppointmentList = appointmentServiceImpl.getAttendeesAppointment(email, appointmentId);
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(userAppointmentList));
        }catch (AppointmentException.AppointmentNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(objectMapper.writeValueAsString(e.getMessage()));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }

    }

}
