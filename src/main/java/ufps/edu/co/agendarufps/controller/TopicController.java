package ufps.edu.co.agendarufps.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.*;
import ufps.edu.co.agendarufps.dtos.TopicDto;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.service.exception.TopicException;
import ufps.edu.co.agendarufps.service.impl.TopicServiceImpl;
import ufps.edu.co.agendarufps.service.impl.UserServiceImpl;
import ufps.edu.co.agendarufps.utils.MessageConstants;


@RestController
@RequiredArgsConstructor
public class TopicController {
    private final ObjectMapper objectMapper;
    private final TopicServiceImpl topicServiceImpl;
    private final UserServiceImpl userServiceImpl;

    @PostMapping(value = "/topic/", produces = "application/json")
    public ResponseEntity<String> createTopic(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user,
                                              @RequestBody TopicDto topicDto) throws JsonProcessingException {
        try{
            if(topicDto != null){
                topicDto.setEmailCreatedBy(user.getAttribute("email"));
                TopicDto topic = topicServiceImpl.saveTopic(topicDto);
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(topic));
            }
        }catch (TopicException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.TOPIC_ERROR));
        }
        return null;
    }

    @PutMapping(value = "topic/", produces = "application/json")
    public ResponseEntity<String> updateTopic(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user,
            @RequestBody TopicDto topicDto) throws JsonProcessingException {
        try {
            if (topicDto.getName() == null || topicDto.getName().isBlank()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.TOPIC_NAME_IS_REQUIRED));
            }
            TopicDto topic = topicServiceImpl.getTopic(topicDto.getId());
            TopicDto newTopic = topicServiceImpl.updateTopic(topicDto);
            return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(newTopic));
        }catch (TopicException.TopicNotFoundException | JsonProcessingException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(e.getMessage()));
        }catch (TopicException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }
    }

    @GetMapping(value = "topic/{idTopic}", produces = "application/json")
    public ResponseEntity<String> getTopic(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user,
                                           @PathVariable int idTopic) throws JsonProcessingException {
        try {
            TopicDto topic = topicServiceImpl.getTopic(idTopic);
            if (topic.getEmailCreatedBy() == user.getAttribute("email")) {
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(topic));
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.TOPIC_NOT_FOUND));
            }
        }catch (TopicException.TopicNotFoundException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(e.getMessage()));
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }
    }

    @GetMapping(value = "topic/", produces = "application/json")
    public ResponseEntity<String> getAllTopicByUserEmail(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user) throws JsonProcessingException {
        User u = userServiceImpl.findUserByEmail(user.getAttribute("email"));
        return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(topicServiceImpl.getAllTopicByUserEmail(u.getEmail())));
    }

    @DeleteMapping(value = "topic/{idTopic}", produces = "application/json")
    public ResponseEntity<String> deleteTopic(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal user,
                                                  @PathVariable int idTopic) throws JsonProcessingException {
        try {
            TopicDto topic = topicServiceImpl.getTopic(idTopic);
            if (topic != null && topic.getEmailCreatedBy().equals(user.getAttribute("email"))) {
                topicServiceImpl.deleteTopic(topic.getId());
                return ResponseEntity.status(HttpStatus.OK).body(objectMapper.writeValueAsString(MessageConstants.TOPIC_DELETE));
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(MessageConstants.TOPIC_NOT_FOUND));
            }
        } catch (TopicException.TopicInUseException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(objectMapper.writeValueAsString(e.getMessage()));
        } catch (TopicException.TopicNotFoundException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(objectMapper.writeValueAsString(e.getMessage()));
        }
    }
}
