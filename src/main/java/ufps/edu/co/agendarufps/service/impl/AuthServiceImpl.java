package ufps.edu.co.agendarufps.service.impl;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.util.Utils;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.config.CalendarConfig;
import ufps.edu.co.agendarufps.config.SecurityConfig;
import ufps.edu.co.agendarufps.dtos.UrlDto;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.mapping.UserMapping;
import ufps.edu.co.agendarufps.service.IAuthService;

import java.io.IOException;

@Service
@AllArgsConstructor
public class AuthServiceImpl implements IAuthService {


    private final SecurityConfig tokenIntrospector;
    private final CalendarConfig calendarConfig;

    private final UserServiceImpl userServiceImpl;

    @Override
    public UrlDto getUrlAuthorization() {
        try {
            String url = new GoogleAuthorizationCodeRequestUrl(
                    calendarConfig.getGoogleClientId(),
                    calendarConfig.getRedirectUri(),
                    calendarConfig.getScopes())
                    .build();
            return new UrlDto(url);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String generateToken(String code) {
        HttpTransport httpTransport = Utils.getDefaultTransport();
        String token;
        try {
            token = new GoogleAuthorizationCodeTokenRequest(
                    httpTransport, new GsonFactory(),
                    calendarConfig.getGoogleClientId(),
                    calendarConfig.getGoogleClientSecret(),
                    code,
                    calendarConfig.getRedirectUri()
            ).execute().getAccessToken();
            // Introspect the token to get user information
            OAuth2AuthenticatedPrincipal userPrincipal = tokenIntrospector.introspector().introspect(token);
            User user = userServiceImpl.findUserByEmail(userPrincipal.getAttributes().get("email").toString());
            if (user == null) {
                user = new UserMapping().userPrincipalToUser(userPrincipal);
                userServiceImpl.saveUser(user);
            }
            if (user != null && !userServiceImpl.validateFullData(user)){
                user = new UserMapping().userPrincipalToUser(userPrincipal);
                userServiceImpl.updateUser(user);
            }
            return token;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
