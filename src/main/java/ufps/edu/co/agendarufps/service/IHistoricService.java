package ufps.edu.co.agendarufps.service;

import ufps.edu.co.agendarufps.entity.Appointment;
import ufps.edu.co.agendarufps.entity.Historic;

import java.time.LocalDate;
import java.util.List;

public interface IHistoricService {

    void saveHistoric(Appointment historic);
    List<Historic> getHistoricByEmailOrganizer(String email);
    List<Historic> getHistoricByEmailOrganizerAndPeriod(String email, String period);
    List<Historic> getHistoricBetweenDate(LocalDate start, LocalDate end);
}
