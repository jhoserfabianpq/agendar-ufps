package ufps.edu.co.agendarufps.service;

import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.WeekDayDto;
import ufps.edu.co.agendarufps.entity.Availability;
import ufps.edu.co.agendarufps.entity.WeekDay;

import java.util.List;


@Service
public interface IWeekDayService {
    WeekDayDto saveWeekDay(WeekDayDto eventEntity);
    WeekDayDto getWeekDay(int idWeekDay);
    List<WeekDay> updateWeekDay(Availability availability, List<WeekDayDto> eventEntity);
    void deleteWeekDay(int idWeekDay);

}
