package ufps.edu.co.agendarufps.service;

import ufps.edu.co.agendarufps.dtos.IntervalDto;
import ufps.edu.co.agendarufps.entity.Interval;

public interface IIntervalsService {
    IntervalDto saveInterval(IntervalDto interval);
    IntervalDto getInterval(int idInterval);

}
