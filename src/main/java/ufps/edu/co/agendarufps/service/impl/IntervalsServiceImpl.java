package ufps.edu.co.agendarufps.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.IntervalDto;
import ufps.edu.co.agendarufps.dtos.WeekDayDto;
import ufps.edu.co.agendarufps.entity.Interval;
import ufps.edu.co.agendarufps.entity.WeekDay;
import ufps.edu.co.agendarufps.mapping.IntervalMapping;
import ufps.edu.co.agendarufps.repository.IIntervalRepository;
import ufps.edu.co.agendarufps.service.IIntervalsService;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class IntervalsServiceImpl implements IIntervalsService {
    private final IIntervalRepository intervalsRepository;
    @Override
    public IntervalDto saveInterval(IntervalDto interval) {
        return IntervalMapping.intervalEntityToIntervalDto(intervalsRepository.saveAndFlush(IntervalMapping.intervalDtoToIntervalDEntity(interval)));
    }

    @Override
    public IntervalDto getInterval(int idInterval) {
        Optional<Interval> findInterval = intervalsRepository.findById(idInterval);
        return IntervalMapping.intervalEntityToIntervalDto(findInterval.orElse(null)) ;
    }

    /*public List<IntervalDto> getIntervalByWeekDay(IntervalDto wd, int idEvent) {

    }*/

}
