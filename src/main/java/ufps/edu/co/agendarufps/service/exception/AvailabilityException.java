package ufps.edu.co.agendarufps.service.exception;

public class AvailabilityException extends RuntimeException {

    public AvailabilityException (String message){
        super(message);
    }
    public static class AvailabilityInUseException extends RuntimeException {
        public AvailabilityInUseException(String message) {
            super(message);
        }
    }

    public static class AvailabilityNotFoundException extends RuntimeException {
        public AvailabilityNotFoundException(String message) {
            super(message);
        }
    }
}
