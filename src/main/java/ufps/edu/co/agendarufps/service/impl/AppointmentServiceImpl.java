package ufps.edu.co.agendarufps.service.impl;

import com.google.api.services.calendar.model.Event;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ufps.edu.co.agendarufps.dtos.*;
import ufps.edu.co.agendarufps.entity.*;
import ufps.edu.co.agendarufps.mapping.*;
import ufps.edu.co.agendarufps.repository.IAppointmentRepository;
import ufps.edu.co.agendarufps.repository.IEventRepository;
import ufps.edu.co.agendarufps.repository.IUserAppointmentRepository;
import ufps.edu.co.agendarufps.service.IAppointmentService;
import ufps.edu.co.agendarufps.service.exception.AppointmentException;
import ufps.edu.co.agendarufps.service.exception.EventException;
import ufps.edu.co.agendarufps.service.exception.TopicException;
import ufps.edu.co.agendarufps.utils.MessageConstants;
import ufps.edu.co.agendarufps.utils.Utilities;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AppointmentServiceImpl implements IAppointmentService {
    private final UserServiceImpl userServiceImpl;
    private final IEventRepository eventRepository;
    private final IAppointmentRepository appointmentRepository;
    private final TopicServiceImpl topicServiceImpl;
    private final HistoricServiceImpl historyServiceImpl;
    private final IUserAppointmentRepository userAppointmentRepository;
    private final CalendarServiceImpl calendarServiceImpl;

    @Override
    @Transactional
    public AppointmentDto saveAppointment(AppointmentDto appointment) throws IOException {
        Optional<EventEntity> event = eventRepository.findById(appointment.getEventEntity().getId());
        if(event.isPresent() && event.get().getState() == 1){
            Appointment appointmentEntity = new Appointment();
            User newUser = userServiceImpl.findUserByEmail(event.get().getUser().getEmail());
            if (newUser == null) {
                throw new EventException.EventNotFoundException(MessageConstants.USER_NOT_FOUND);
            }
            TopicDto topic = topicServiceImpl.getTopic(appointment.getTopic().getId());
            if (topic == null) {
                throw new TopicException.TopicNotFoundException(MessageConstants.TOPIC_NOT_FOUND);
            }
            event.get().setUser(newUser);
            appointmentEntity.setLocation(appointment.getLocation());
            appointmentEntity.setPeriod(Utilities.getPeriod(LocalDateTime.now()));
            appointmentEntity.setCreatedAt(LocalDateTime.now());
            appointmentEntity.setAppointmentStart(appointment.getAppointmentStart());
            appointmentEntity.setAppointmentEnd(buildDateEnd(event.get(),appointment.getAppointmentStart()));
            appointmentEntity.setTopic(TopicMapping.topicDtoToTopicEntity(topic));
            appointmentEntity.setComments(appointment.getComments());
            appointmentEntity.setName(new Utilities().buildNameAppointment(topic.getName(),
                    EventMapping.eventEntityToEventDto(event.get())));
            appointmentEntity.setEventEntity(event.get());
            List<UserDto> listInvitedUser = userServiceImpl.buildInvitedUsers(newUser,appointment.getInvitedUsers());
            appointmentEntity.setInvitedUsers(UserMapping.listUserDtoToUserEntity(listInvitedUser));
            try {
                Event e = calendarServiceImpl.insertEventAppointment(appointmentEntity);
                appointmentEntity.setUrlMeet(e.getHangoutLink());
                appointmentEntity.setIdGoogleCalendar(e.getId());
                appointmentEntity.setLinkGoogleCalendar(e.getHtmlLink());
            }catch (AppointmentException error){
                throw new AppointmentException.AppointmentGoogleCalendar(error.getMessage());
            }
            Appointment newAppointment = appointmentRepository.saveAndFlush(appointmentEntity);
            newAppointment.setUserAppointments(userServiceImpl.buildUserAppointments(listInvitedUser, newAppointment));
            historyServiceImpl.saveHistoric(newAppointment);
            return AppointmentMapping.appointmentEntityToAppointmentDto(newAppointment);
        }else{
            throw new EventException.EventNotFoundException(MessageConstants.EVENT_NOT_FOUND);
        }
    }
    private LocalDateTime buildDateEnd(EventEntity event, LocalDateTime start) {
        return start.plusMinutes(event.getDuration());
    }

    @Override
    public List<AppointmentDto> getAllAppointmentUpcoming(User user) {
        List<Appointment> appointments = appointmentRepository.
                findByInvitedUsersEmailAndAppointmentStartAfterOrderByAppointmentStartAsc(
                        user.getEmail(), (LocalDateTime.now()).toLocalDate().atStartOfDay()
                );
        appointments.removeIf(invitedAppointment -> invitedAppointment.getEventEntity().getIsDeleted());
        List<AppointmentDto> appointmentDtos = AppointmentMapping.listAppointmentEntityToAppointmentDto(appointments);
        return appointmentDtos;
    }
    @Override
    public List<AppointmentDto> getAllAppointment(User user) {
        List<Appointment> invitedAppointments = appointmentRepository.
                findByInvitedUsersEmailOrderByAppointmentStartAsc(user.getEmail());
        List<AppointmentDto> invitedAppointmentDtos = AppointmentMapping.listAppointmentEntityToAppointmentDto(invitedAppointments);
        return invitedAppointmentDtos;
    }


    public List<AppointmentDto> getAllAppointmentInvite(User user) {
        List<Appointment> invitedAppointments = appointmentRepository
                .findByInvitedUsersEmailOrderByAppointmentStartAsc(user.getEmail());
        List<Appointment> myCreatedAppointments = appointmentRepository
                .findByEventEntityUserEmailOrderByAppointmentStartAsc(user.getEmail());
        invitedAppointments.removeIf(invitedAppointment -> myCreatedAppointments.contains(invitedAppointment) );
        invitedAppointments.removeIf(invitedAppointment -> invitedAppointment.getEventEntity().getIsDeleted());
        List<AppointmentDto> invitedAppointmentDtos = AppointmentMapping.listAppointmentEntityToAppointmentDto(invitedAppointments);
        return invitedAppointmentDtos;
    }

    @Override
    public List<AttendeesAppointmentDto> getAllMyAppointmentCreator(User user) {
        List<Appointment> appointments = appointmentRepository.findByEventEntityUserEmailOrderByAppointmentStartAsc(user.getEmail());
        List<AttendeesAppointmentDto> attendeesAppointmentDtos = new ArrayList<>();

        for (Appointment appointment : appointments) {
            AttendeesAppointmentDto attendeesAppointmentDto = AttendeesAppointmentDto.builder()
                    .appointment(AppointmentMapping.appointmentEntityToAppointmentDto(appointment))
                    .build();
            attendeesAppointmentDtos.add(attendeesAppointmentDto);
        }
        attendeesAppointmentDtos.removeIf(invitedAppointment -> invitedAppointment.getAppointment().getEventEntity().getIsDeleted());
        return attendeesAppointmentDtos;
    }

    public List<AttendeesAppointmentDto> getAllAppointmentByPeriod(User user, String period) {
        List<Appointment> appointments = appointmentRepository.findAllByInvitedUsersEmailAndPeriod(user.getEmail(), period);
        List<AttendeesAppointmentDto> attendeesAppointmentDtos = new ArrayList<>();
        for (Appointment appointment : appointments) {
            AttendeesAppointmentDto attendeesAppointmentDto = AttendeesAppointmentDto.builder()
                    .appointment(AppointmentMapping.appointmentEntityToAppointmentDto(appointment))
                    .build();
            attendeesAppointmentDtos.add(attendeesAppointmentDto);
        };
        return attendeesAppointmentDtos;
    }

    @Transactional
    public void markAttendance(int appointmentId, long userId, boolean attended) {
        UserAppointmentId userAppointmentId = new UserAppointmentId(userId, appointmentId);
        Optional<UserAppointment> userAppointmentOptional = userAppointmentRepository.findById(userAppointmentId);
        if (userAppointmentOptional.isPresent()) {
            UserAppointment userAppointment = userAppointmentOptional.get();
            userAppointment.setAttended(attended);
            userAppointmentRepository.save(userAppointment);
        } else {
            throw new AppointmentException.AppointmentNotFoundException("Error en el appointmentId o userId");
        }
    }

    public AttendeesAppointmentDto getAttendeesAppointment(String email, int appointmentId) {
        Optional<Appointment> appointment = appointmentRepository.findById(appointmentId);
        if (appointment.isPresent() &&
                appointment.get().getEventEntity().getUser().getEmail().equals(email)) {
            AttendeesAppointmentDto attendeesAppointmentDto = new AttendeesAppointmentDto();
            AppointmentDto appointmentDto = AppointmentMapping.appointmentEntityToAppointmentDto(appointment.get());
            attendeesAppointmentDto.setAppointment(appointmentDto);
            return attendeesAppointmentDto;
        }else{
            throw new AppointmentException.AppointmentNotFoundException(MessageConstants.APPOINTMENT_NOT_FOUND);
        }
    }
}
