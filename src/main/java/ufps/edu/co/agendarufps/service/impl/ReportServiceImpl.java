package ufps.edu.co.agendarufps.service.impl;

import jakarta.persistence.Column;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.GeneralReportDto;
import ufps.edu.co.agendarufps.entity.*;
import ufps.edu.co.agendarufps.mapping.AppointmentMapping;
import ufps.edu.co.agendarufps.mapping.EventMapping;
import ufps.edu.co.agendarufps.repository.IAppointmentRepository;
import ufps.edu.co.agendarufps.service.exception.AppointmentException;
import ufps.edu.co.agendarufps.utils.MessageConstants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ReportServiceImpl {

    private final UserServiceImpl userService;
    private final EventServiceImpl eventService;
    private final IAppointmentRepository appointmentRepository;

    public byte[] generateExcelReport(List<Historic> historicList) throws IOException {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream  outputStream = new ByteArrayOutputStream ()) {

            Sheet sheet = workbook.createSheet("Historic Report");

            // Crear fila de encabezado
            Row headerRow = sheet.createRow(0);

            // Obtener campos de la entidad Historic
            List<Field> fieldList = Arrays.asList(Historic.class.getDeclaredFields());

            // Excluir el primer campo el id
            List<Field> fieldsToInclude = fieldList.subList(1, fieldList.size());

            // Convertir la lista resultante nuevamente en un array
            Field[] fields = fieldsToInclude.toArray(new Field[0]);

            // Crear encabezados dinámicamente
            int columnNum = 0;
            for (Field field : fields) {
                if (field.isAnnotationPresent(Column.class)) {
                    String headerName = field.getName().toUpperCase();
                    headerRow.createCell(columnNum++).setCellValue(headerName);
                }
            }

            int rowNum = 1;
            for (Historic historic : historicList) {
                Row row = sheet.createRow(rowNum++);
                int cellNum = 0;
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        field.setAccessible(true);
                        try {
                            Object value = field.get(historic);
                            if (value instanceof LocalDateTime) {
                                row.createCell(cellNum++).setCellValue(value.toString());
                            } else {
                                row.createCell(cellNum++).setCellValue(String.valueOf(value));
                            }
                        } catch (IllegalAccessException e) {
                            throw new IllegalStateException(MessageConstants.REPORT_ERROR_HISTORIC);
                        }
                    }
                }
            }

            byte[] byteArray;
            workbook.write(outputStream);
            byteArray = outputStream.toByteArray();
            workbook.close();

            return byteArray;
        }catch (IOException e) {
            throw new IOException(MessageConstants.REPORT_ERROR);
        }
    }

    public List<GeneralReportDto> getGeneralReport(String email, String period) {
        User emailOrganizer = userService.findUserByEmail(email);
        List<GeneralReportDto> generalReportDtos = new ArrayList<>();
        if(emailOrganizer == null) throw new AppointmentException(MessageConstants.USER_NOT_FOUND);
        List<EventEntity> eventEntities = eventService.getAllEventByUserEmailAndPeriod(emailOrganizer.getEmail(), period);
        for (EventEntity eventEntity : eventEntities) {
            List<Topic> topics = eventEntity.getTopics();
            for (Topic topic : topics) {
                int totalInvitedUsers = 0;
                int totalAttendedUsers = 0;
                int virtualAppointments = 0;
                int faceToFaceAppointments = 0;
                int averageTime = 0;
                List<Appointment> appointmentsByTopic = appointmentRepository.
                        findAllByTopicIdAndEventEntityIdAndPeriod(topic.getId(), eventEntity.getId(), period);
                for (Appointment appointment : appointmentsByTopic) {
                    totalInvitedUsers += appointment.getInvitedUsers().size();
                    totalAttendedUsers += appointment.getUserAppointments().stream()
                            .filter(UserAppointment::isAttended)
                            .count();
                    averageTime += appointment.getAppointmentStart().until(appointment.getAppointmentEnd(), ChronoUnit.MINUTES);
                    if (appointment.getLocation().equalsIgnoreCase("Meet")) virtualAppointments++;
                    else faceToFaceAppointments++;
                }
                if (!appointmentsByTopic.isEmpty()) {averageTime /= appointmentsByTopic.size();}
                GeneralReportDto report = new GeneralReportDto();
                report.setAverageTime(averageTime);
                report.setCountAppointmentXTopic(appointmentsByTopic.size());
                report.setTotalAttendedUsers(totalAttendedUsers);
                report.setTotalMissedUsers(totalInvitedUsers - totalAttendedUsers);
                report.setVirtualAppointments(virtualAppointments);
                report.setInPersonAppointments(faceToFaceAppointments);
                report.setEventName(eventEntity.getName());
                report.setTopicName(topic.getName());
                generalReportDtos.add(report);
            }
        }
        return generalReportDtos;
    }
}
