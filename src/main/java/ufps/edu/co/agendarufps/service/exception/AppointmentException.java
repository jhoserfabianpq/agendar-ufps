package ufps.edu.co.agendarufps.service.exception;

public class AppointmentException extends RuntimeException{
    public AppointmentException(String message) {
        super(message);
    }
    public static class AppointmentNotFoundException extends RuntimeException {
        public AppointmentNotFoundException(String message) {
            super(message);
        }
    }

    public static class AppointmentGoogleCalendar extends RuntimeException {
        public AppointmentGoogleCalendar(String message) {
            super(message);
        }
    }
}
