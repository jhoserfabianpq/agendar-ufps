package ufps.edu.co.agendarufps.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.entity.Appointment;
import ufps.edu.co.agendarufps.entity.Historic;
import ufps.edu.co.agendarufps.repository.IHistoricRepository;
import ufps.edu.co.agendarufps.service.IHistoricService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Service
@AllArgsConstructor
public class HistoricServiceImpl implements IHistoricService {
    private final IHistoricRepository historicRepository;
    private final Integer SEMESTER_LIMIT = 6;

    @Override
    public void saveHistoric(Appointment appointment) {
        Historic historic = new Historic();
        historic.setAttendees(buildAttendees(appointment));
        historic.setDuration(appointment.getEventEntity().getDuration());
        historic.setDescription(appointment.getEventEntity().getDescription());
        historic.setEmailOrganizer(appointment.getEventEntity().getUser().getEmail());
        historic.setCreatedAt(appointment.getCreatedAt());
        historic.setEventName(appointment.getEventEntity().getName());
        historic.setTopics(appointment.getTopic().getName());
        historic.setDateStart(appointment.getAppointmentStart());
        historic.setDateEnd(appointment.getAppointmentEnd());
        historic.setPeriod(buildPeriod(appointment.getAppointmentStart()));
        historicRepository.save(historic);
    }

    @Override
    public List<Historic> getHistoricByEmailOrganizer(String email) {
        return historicRepository.findHistoricByEmailOrganizer(email);
    }

    @Override
    public List<Historic> getHistoricByEmailOrganizerAndPeriod(String email, String period) {
        return historicRepository.findHistoricByEmailOrganizerAndPeriod(email, period);
    }

    @Override
    public List<Historic> getHistoricBetweenDate(LocalDate start, LocalDate end) {
        LocalDateTime startOfDay = start.atStartOfDay();
        LocalDateTime endOfDay = end.atTime(LocalTime.MAX);
        return historicRepository.findHistoricByDateStartBetween(startOfDay, endOfDay);
    }

    private String buildAttendees(Appointment appointment) {
        return appointment.getInvitedUsers().stream().map(user -> user.getEmail()).reduce((email1, email2) -> email1 + ", " + email2).orElse("");
    }
    private String buildPeriod(LocalDateTime start) {
        String year = start.getYear()+"";
        String semester = (start.getMonthValue() <= SEMESTER_LIMIT) ? "01" : "02";
        return year + semester;
    }
}
