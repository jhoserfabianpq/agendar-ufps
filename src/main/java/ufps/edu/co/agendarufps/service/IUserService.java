package ufps.edu.co.agendarufps.service;

import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.entity.User;

import java.util.List;

@Service
public interface IUserService {
    User saveUser(User user);
    User updateUser(User user);
    User findUserByEmail(String email);
    User findByUsernameWithoutDomain(String username);
    void deleteUser(String email);
    List<User> findAll();
}
