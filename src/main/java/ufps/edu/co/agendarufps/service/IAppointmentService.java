package ufps.edu.co.agendarufps.service;

import ufps.edu.co.agendarufps.dtos.AppointmentDto;
import ufps.edu.co.agendarufps.dtos.AttendeesAppointmentDto;
import ufps.edu.co.agendarufps.entity.User;

import java.io.IOException;
import java.util.List;

public interface IAppointmentService {
    AppointmentDto saveAppointment(AppointmentDto appointment) throws IOException;
    List<AppointmentDto> getAllAppointmentUpcoming(User user) throws IOException;
    List<AppointmentDto> getAllAppointment(User user) throws IOException;
    List<AttendeesAppointmentDto> getAllMyAppointmentCreator(User user) throws IOException;
}
