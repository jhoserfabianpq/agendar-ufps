package ufps.edu.co.agendarufps.service;

import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.AppointmentDto;
import ufps.edu.co.agendarufps.entity.Appointment;

import java.io.IOException;

@Service
public interface ICalendarService {

    Calendar getCalendar(String token) ;
    Event insertEventAppointment(Appointment appointment) throws IOException;
    Event getEventGoogle(String eventId)throws IOException;
}