package ufps.edu.co.agendarufps.service;

import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.UrlDto;

@Service
public interface IAuthService {
    UrlDto getUrlAuthorization();
    String generateToken(String code);
}
