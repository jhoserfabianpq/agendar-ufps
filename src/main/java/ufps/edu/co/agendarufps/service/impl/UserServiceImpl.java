package ufps.edu.co.agendarufps.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.UserDto;
import ufps.edu.co.agendarufps.entity.Appointment;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.entity.UserAppointment;
import ufps.edu.co.agendarufps.entity.UserAppointmentId;
import ufps.edu.co.agendarufps.mapping.UserMapping;
import ufps.edu.co.agendarufps.repository.IUserAppointmentRepository;
import ufps.edu.co.agendarufps.repository.IUserRepository;
import ufps.edu.co.agendarufps.service.IUserService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserServiceImpl implements IUserService {

    private final IUserRepository userRepository;
    private final IUserAppointmentRepository userAppointmentRepository;
    @Override
    public User saveUser(User user) {
        User newUser = findUserByEmail(user.getEmail());
        if(newUser != null){
            return newUser;
        }
        return userRepository.saveAndFlush(user);
    }

    /*
     * Validate that the user has the email registered in some appointment, validate the other data if they are empty
     * with this method validate the above to be able to complete the remaining data from Google
     **/
    public boolean validateFullData(User user){
        return !isNullOrEmpty(user.getPictureUrl()) &&
                !isNullOrEmpty(user.getLastName()) &&
                !isNullOrEmpty(user.getFirstName());
    }

    private boolean isNullOrEmpty(String value) {
        return value == null || value.isBlank();
    }

    @Override
    public User updateUser(User user) {
        User u = findUserByEmail(user.getEmail());
        if(u != null){
            u.setPictureUrl(user.getPictureUrl());
            u.setFirstName(user.getFirstName());
            u.setLastName(user.getLastName());
            u.setUpdatedAt(LocalDateTime.now());
            return userRepository.saveAndFlush(u);
        }else{
            return null;
        }
    }

    @Override
    public User findUserByEmail(String email) {
        User u = userRepository.findByEmail(email);
        return u;
    }

    @Override
    public User findByUsernameWithoutDomain(String username) {
        return userRepository.findByUsernameWithoutDomain(username);
    }

    @Override
    public void deleteUser(String email) {
        userRepository.delete(findUserByEmail(email));
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    public List<UserDto> buildInvitedUsers(User userEvent, List<UserDto> invitedUsers) {
        UserDto eventCreator = new UserDto();
        eventCreator.setEmail(userEvent.getEmail());
        invitedUsers.add(eventCreator);
        List<UserDto> invitedUsersList = new ArrayList<>();
        Set<String> setEmails = invitedUsers.stream()
                .map(UserDto::getEmail)
                .collect(Collectors.toSet());
        for (String email : setEmails) {
            User u = findUserByEmail(email);
            if (u == null) {
                User newUser = new User();
                newUser.setEmail(email);
                newUser.setCreatedAt(LocalDateTime.now());
                newUser.setUpdatedAt(LocalDateTime.now());
                u = saveUser(newUser);
            }
            invitedUsersList.add(UserMapping.userEntityToUserDto(u));
        }
        return invitedUsersList;
    }

    public List<UserAppointment> buildUserAppointments(List<UserDto> invitedUsers, Appointment appointment) {
        List<UserAppointment> invitedUsersList = new ArrayList<>();
        for (UserDto user : invitedUsers) {
            User u = findUserByEmail(user.getEmail());
            UserAppointmentId userAppointId = new UserAppointmentId(user.getId(), appointment.getId());
            UserAppointment userAppoint =new UserAppointment(userAppointId, u, appointment,false);
            invitedUsersList.add(userAppoint);
            userAppointmentRepository.save(userAppoint);
        }
        //userAppointmentRepository.saveAll(invitedUsersList);
        return invitedUsersList;
    }
}
