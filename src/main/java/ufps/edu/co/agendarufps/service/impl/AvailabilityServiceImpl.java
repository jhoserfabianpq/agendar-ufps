package ufps.edu.co.agendarufps.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ufps.edu.co.agendarufps.dtos.AvailabilityDto;
import ufps.edu.co.agendarufps.dtos.WeekDayDto;
import ufps.edu.co.agendarufps.entity.Availability;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.entity.WeekDay;
import ufps.edu.co.agendarufps.mapping.AvailabilityMapping;
import ufps.edu.co.agendarufps.mapping.UserMapping;
import ufps.edu.co.agendarufps.repository.IAvailabilityRepository;
import ufps.edu.co.agendarufps.service.IAvailabilityService;
import ufps.edu.co.agendarufps.service.exception.AvailabilityException;
import ufps.edu.co.agendarufps.utils.MessageConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AvailabilityServiceImpl implements IAvailabilityService {

    private final IAvailabilityRepository availabilityRepository;
    private final WeekDayServiceImpl weekDayService;
    private final UserServiceImpl userService;
    @Override
    @Transactional
    public AvailabilityDto saveAvailability(AvailabilityDto availability, String email) {
        try {
            if (availability.getId() != null) {
                Optional<Availability> findAvailability = availabilityRepository.findById(availability.getId());
                if (findAvailability.isPresent()) {
                    return AvailabilityMapping.availabilityEntityToAvailabilityDto(findAvailability.get());
                }
            } else {
                User user = userService.findUserByEmail(email);
                availability.setCreatedBy(UserMapping.userEntityToUserDto(user));
                Availability createdAvailability = availabilityRepository.saveAndFlush(AvailabilityMapping.availabilityDtoToAvailabilityEntity(availability));
                AvailabilityDto updatedAvailability = AvailabilityMapping.availabilityEntityToAvailabilityDto(createdAvailability);
                if (!availability.getWeekDays().isEmpty()){
                    List<WeekDayDto> weekDays = new ArrayList<>();
                    for (WeekDayDto weekDayDto : availability.getWeekDays()) {
                        weekDayDto.setAvailability(updatedAvailability);
                        WeekDayDto createdWeekDay = weekDayService.saveWeekDay(weekDayDto);
                        weekDays.add(createdWeekDay);
                    }
                    updatedAvailability.setWeekDays(weekDays);
                }
                    return updatedAvailability;
            }
        }catch (Exception e){
            throw new AvailabilityException(MessageConstants.AVAILABILITY_ERROR);
        }
        return null;
    }

    @Override
    public Availability getAvailability(Long idAvailability) {
        Optional<Availability> findAvailability = availabilityRepository.findById(idAvailability);
        if (findAvailability.isPresent()) {
            return findAvailability.get();
        } else {
            throw new AvailabilityException.AvailabilityNotFoundException(MessageConstants.AVAILABILITY_NOT_FOUND);
        }
    }

    @Override
    public List<AvailabilityDto> getAllAvailability(String email){
        List<Availability> availabilityList = availabilityRepository.findAllByCreatedByEmail(email);
        if(availabilityList.isEmpty()) return new ArrayList<>();
        return AvailabilityMapping.listAvailabilityDtoToAvailabilityEntity(availabilityList);
    }

    @Override
    @Transactional
    public AvailabilityDto updateAvailability(AvailabilityDto availability) {
        try {
            Optional<Availability> findAvailability = availabilityRepository.findById(availability.getId());
            if (findAvailability.isPresent()) {
                findAvailability.get().setName(availability.getName());
                List<WeekDay> updatesWeekDays = weekDayService.updateWeekDay(findAvailability.get(),availability.getWeekDays());
                findAvailability.get().getWeekDays().clear();
                findAvailability.get().setWeekDays(updatesWeekDays);
                return AvailabilityMapping.availabilityEntityToAvailabilityDto(availabilityRepository.saveAndFlush(findAvailability.get()));
            }else{
                throw new AvailabilityException.AvailabilityNotFoundException(MessageConstants.AVAILABILITY_NOT_FOUND);
            }
        } catch (Exception e){
            throw new AvailabilityException(MessageConstants.AVAILABILITY_ERROR);
        }
    }

    @Override
    public void deleteAvailability(Long idAvailability) {
        Optional<Availability> availability = availabilityRepository.findById(idAvailability);
        if (availability.isPresent()){
            if (availability.get().getEventEntity().isEmpty()) {
                availabilityRepository.deleteById(idAvailability);
            }else{
                throw new AvailabilityException.AvailabilityInUseException(MessageConstants.AVAILABILITY_IN_USE);
            }
        }else{
            throw new AvailabilityException.AvailabilityNotFoundException(MessageConstants.AVAILABILITY_NOT_FOUND);
        }
    }
}
