package ufps.edu.co.agendarufps.service;

import ufps.edu.co.agendarufps.dtos.TopicDto;
import java.util.List;

public interface ITopicService {
    TopicDto saveTopic(TopicDto topic);
    TopicDto getTopic(int idTopic);
    List<TopicDto> getAllTopicByUserEmail(String email);
    void deleteTopic(int idTopic);
    TopicDto updateTopic(TopicDto topic);

}
