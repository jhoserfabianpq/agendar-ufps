package ufps.edu.co.agendarufps.service;

import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.entity.EventEntity;

import java.util.List;

@Service
public interface IEventService {
    EventEntity saveEvent(EventEntity eventEntity);
    EventEntity getEventByNameAndEmail(String name, String email);
    List<EventEntity> getAllEventByUserEmail(String userEmail);
    EventEntity updateEvent(EventEntity eventEntity);
    void deleteEvent(Long idEvent);

}
