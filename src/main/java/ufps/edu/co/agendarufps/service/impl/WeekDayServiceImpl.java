package ufps.edu.co.agendarufps.service.impl;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.IntervalDto;
import ufps.edu.co.agendarufps.dtos.WeekDayDto;
import ufps.edu.co.agendarufps.entity.*;
import ufps.edu.co.agendarufps.mapping.WeekDayMapping;
import ufps.edu.co.agendarufps.repository.IAvailabilityRepository;
import ufps.edu.co.agendarufps.repository.IWeekDayRepository;
import ufps.edu.co.agendarufps.service.IWeekDayService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class WeekDayServiceImpl implements IWeekDayService {

    private final IWeekDayRepository weekDayRepository;
    private final IntervalsServiceImpl intervalService;
    private final IAvailabilityRepository availabilityRepository;

    @Override
    @Transactional
    public WeekDayDto saveWeekDay(WeekDayDto weekDay) {
        // Crea el día de la semana
        WeekDayDto createdWeekDay = WeekDayMapping.weekDayEntityToWeekDayDto(
                weekDayRepository.saveAndFlush(WeekDayMapping.weekDayDtoToWeekDayEntity(weekDay)));

        // Asocia los intervalos
        List<IntervalDto> intervals = new ArrayList<>();
        for (IntervalDto intervalDto : weekDay.getIntervals()) {
            intervalDto.setWeekDay(createdWeekDay);
            IntervalDto createdInterval = intervalService.saveInterval(intervalDto);
            intervals.add(createdInterval);
        }

        createdWeekDay.setIntervals(intervals);
        return createdWeekDay;
    }


    @Override
    public WeekDayDto getWeekDay(int idWeekDay) {
        Optional<WeekDay> findWeekDay = weekDayRepository.findById(idWeekDay);
        return WeekDayMapping.weekDayEntityToWeekDayDto(findWeekDay.orElse(null));
    }

    public WeekDayDto getWeekDayByNameAndAvailability(String name, long idAvailability) {
        return WeekDayMapping.weekDayEntityToWeekDayDto(weekDayRepository.findByNameAndAvailabilityId(name, idAvailability));
    }

    @Override
    @Transactional
    public List<WeekDay> updateWeekDay(Availability availability, List<WeekDayDto> weekDayDto) {

        weekDayRepository.deleteByAvailabilityId(availability.getId());
        List<WeekDay> weekDays = new ArrayList<>();
        for (WeekDayDto weekDay : weekDayDto) {
            WeekDayDto updateWeekDay = saveWeekDay(weekDay);
            weekDays.add(WeekDayMapping.weekDayDtoToWeekDayEntity(updateWeekDay));
        }
        return weekDays;
    }

    @Override
    public void deleteWeekDay(int idWeekDay) {
        weekDayRepository.deleteById(idWeekDay);
    }
}
