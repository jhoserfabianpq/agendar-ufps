package ufps.edu.co.agendarufps.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.TopicDto;
import ufps.edu.co.agendarufps.entity.Topic;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.mapping.TopicMapping;
import ufps.edu.co.agendarufps.repository.ITopicRepository;
import ufps.edu.co.agendarufps.service.ITopicService;
import ufps.edu.co.agendarufps.service.exception.TopicException;
import ufps.edu.co.agendarufps.utils.MessageConstants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TopicServiceImpl implements ITopicService {

    private final ITopicRepository topicRepository;
    private final UserServiceImpl userService;
    @Override
    public TopicDto saveTopic(TopicDto topic) {

        topic.setCreatedAt(LocalDateTime.now());
        topic.setUpdatedAt(LocalDateTime.now());
        topic.setIsDeleted(false);
        topic.setVersion(1);
        Topic t = TopicMapping.topicDtoToTopicEntity(topic);
        User user = userService.findUserByEmail(topic.getEmailCreatedBy());
        if (user == null) throw new TopicException(MessageConstants.USER_NOT_FOUND);
        t.setCreatedBy(user);
        Topic newTopic = topicRepository.saveAndFlush(t);
        return TopicMapping.topicEntityToTopicDto(newTopic);
    }

    @Override
    public TopicDto getTopic(int idTopic){
        Optional<Topic> topic = topicRepository.findById(idTopic);
        if (topic.isPresent()){
            return TopicMapping.topicEntityToTopicDto(topic.get());
        }
        throw new TopicException.TopicNotFoundException(MessageConstants.TOPIC_NOT_FOUND);
    }

    @Override
    public List<TopicDto> getAllTopicByUserEmail(String email){
        List<Topic> topics = topicRepository.findAllByCreatedByEmail(email);
        if(topics.isEmpty()){
            return new ArrayList<>();
        }
        topics.removeIf(Topic::getIsDeleted);
        return TopicMapping.listTopicEntityToTopicDto(topics);
    }

    @Override
    public void deleteTopic(int idTopic) {
        Optional<Topic> topic = topicRepository.findById(idTopic);
        if (topic.isPresent()){
            if(topic.get().getEvents().isEmpty()){
                topicRepository.deleteById(idTopic);
            }else{
                throw new TopicException.TopicInUseException(MessageConstants.TOPIC_IN_USE);
            }
        }else{
            throw new TopicException.TopicNotFoundException(MessageConstants.TOPIC_NOT_FOUND);
        }
    }

    @Override
    public TopicDto updateTopic(TopicDto topic) {
        Optional<Topic> t = topicRepository.findById(topic.getId());
        if (t.isPresent() && (topic.getName() != null || !topic.getName().isBlank())){
            t.get().setName(topic.getName());
            t.get().setUpdatedAt(LocalDateTime.now());
            return TopicMapping.topicEntityToTopicDto(topicRepository.saveAndFlush(t.get()));
        }
        throw new TopicException.TopicNotFoundException(MessageConstants.TOPIC_NOT_FOUND);
    }
}
