package ufps.edu.co.agendarufps.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ufps.edu.co.agendarufps.dtos.TopicDto;
import ufps.edu.co.agendarufps.entity.Availability;
import ufps.edu.co.agendarufps.entity.EventEntity;
import ufps.edu.co.agendarufps.entity.Topic;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.mapping.TopicMapping;
import ufps.edu.co.agendarufps.repository.IEventRepository;
import ufps.edu.co.agendarufps.repository.IUserRepository;
import ufps.edu.co.agendarufps.service.IEventService;
import ufps.edu.co.agendarufps.service.exception.EventException;
import ufps.edu.co.agendarufps.utils.MessageConstants;
import ufps.edu.co.agendarufps.utils.Utilities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class EventServiceImpl implements IEventService {

    private final IEventRepository eventRepository;
    private final IUserRepository userRepository;
    private final AvailabilityServiceImpl availabilityService;
    private final TopicServiceImpl topicService;

    @Override
    @Transactional
    public EventEntity saveEvent(EventEntity eventEntity) {
        String email = new Utilities().getEmailAddressWithoutDomain(eventEntity.getUser().getEmail());
        EventEntity event = getEventByNameAndEmail(eventEntity.getName(), email);
        if (event != null) {
            throw new EventException.EventAlreadyExistsException(MessageConstants.EVENT_ALREADY_EXISTS);
        }
        eventEntity.setUpdatedAt(LocalDateTime.now());
        eventEntity.setCreatedAt(LocalDateTime.now());
        eventEntity.setPeriod(Utilities.getPeriod(LocalDateTime.now()));
        eventEntity.setIsDeleted(false);
        List<Topic> topics = new ArrayList<>();
        for (Topic topic : eventEntity.getTopics()) {
            TopicDto topicDto = topicService.getTopic(topic.getId());
            if(topicDto!=null) {
                topics.add(TopicMapping.topicDtoToTopicEntity(topicDto));
            }
        }
        eventEntity.setTopics(topics);
        return eventRepository.saveAndFlush(eventEntity);
    }

    @Override
    public EventEntity getEventByNameAndEmail(String name, String email) {
        User user = userRepository.findByUsernameWithoutDomain(email);
        if(user == null) throw new EventException.EventNotFoundException(MessageConstants.USER_NOT_FOUND);
        Optional<EventEntity> event = eventRepository.findByNameAndUserEmail(name, user.getEmail());
        if (event.isPresent() && !event.get().getIsDeleted()) {
            return event.get();
        }else {
            return null;
        }
    }

    @Override
    public List<EventEntity> getAllEventByUserEmail(String userEmail) {
        return eventRepository.findAllByUserEmailAndIsDeleted(userEmail, false);
    }

    public List<EventEntity> getAllEventByUserEmailAndPeriod(String userEmail, String period) {
        return eventRepository.findAllByUserEmailAndIsDeletedAndPeriod(userEmail, false, period);
    }

    public EventEntity getEventById(Long idEvent) {
        Optional<EventEntity> event = eventRepository.findById(idEvent);
        if (event.isPresent() && !event.get().getIsDeleted()) return event.get();
        else throw new EventException.EventNotFoundException(MessageConstants.EVENT_NOT_FOUND);
    }

    @Override
    @Transactional
    public EventEntity updateEvent(EventEntity eventEntity) {
        EventEntity event = getEventById(eventEntity.getId());
        if(event==null || event.getIsDeleted()) throw new EventException.EventNotFoundException(MessageConstants.EVENT_NOT_FOUND);
        try {
            event.setTypeMeeting(eventEntity.getTypeMeeting());
            event.setName(eventEntity.getName());
            event.setDescription(eventEntity.getDescription());
            event.setDuration(eventEntity.getDuration());
            event.setLocation(eventEntity.getLocation());
            event.setState(eventEntity.getState());
            event.setUpdatedAt(LocalDateTime.now());
            Availability availability = (eventEntity.getAvailability() == null) ? event.getAvailability() :
                    availabilityService.getAvailability(eventEntity.getAvailability().getId());
            List<Topic> topics = (eventEntity.getTopics().isEmpty()) ? event.getTopics() : eventEntity.getTopics();
            event.setAvailability(availability);
            event.setTopics(topics);
        } catch (Exception e) {
            throw new EventException(MessageConstants.EVENT_ERROR);
        }
        return eventRepository.saveAndFlush(event);
    }

    @Override
    public void deleteEvent(Long idEvent) {
        EventEntity event = getEventById(idEvent);
        if (event==null){
            throw new EventException.EventNotFoundException(MessageConstants.EVENT_NOT_FOUND);
        }
        eventRepository.delete(event);
    }

    @Transactional
    public void procedureCleanEventPeriod() {
        // Llama al procedimiento almacenado
        eventRepository.procedureCleanEvent();
    }

}
