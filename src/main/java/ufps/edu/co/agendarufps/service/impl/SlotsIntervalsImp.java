package ufps.edu.co.agendarufps.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.AppointmentDto;
import ufps.edu.co.agendarufps.dtos.CalendarSlotsDto;
import ufps.edu.co.agendarufps.entity.Availability;
import ufps.edu.co.agendarufps.entity.EventEntity;
import ufps.edu.co.agendarufps.entity.Interval;
import ufps.edu.co.agendarufps.entity.WeekDay;
import ufps.edu.co.agendarufps.repository.IEventRepository;
import ufps.edu.co.agendarufps.service.exception.EventException;
import ufps.edu.co.agendarufps.utils.MessageConstants;
import ufps.edu.co.agendarufps.utils.Utilities;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SlotsIntervalsImp {
    private final AppointmentServiceImpl appointmentService;
    private final IEventRepository eventRepository;

    public CalendarSlotsDto getIntervalAvailability(Long idEvent, LocalDate date){
        Optional<EventEntity> eventDto = eventRepository.findById(idEvent);
        if (eventDto.isPresent()){
            return getAvailableSlotsBySpecificDate(eventDto.get().getAvailability(), eventDto.get(), date);
        }
        return new CalendarSlotsDto();
    }


    private CalendarSlotsDto getAvailableSlotsBySpecificDate(Availability availability, EventEntity eventEntity, LocalDate specificDate) {
        List<LocalTime> availableSlots = new ArrayList<>();
        if (availability == null) {
            throw new EventException(MessageConstants.AVAILABILITY_NOT_FOUND);
        }
        if (eventEntity == null) {
            throw new EventException(MessageConstants.EVENT_NOT_FOUND);
        }

        short duration = (eventEntity.getDuration() <= 0) ? 15 : eventEntity.getDuration();

        // Find the WeekDay corresponding to the specific date
        DayOfWeek dayOfWeek = specificDate.getDayOfWeek();
        WeekDay weekDay = findWeekDayByDayOfWeek(availability.getWeekDays(), dayOfWeek);

        if (weekDay != null) {
            List<Interval> intervals = weekDay.getIntervals();
            for (Interval interval : intervals) {
                LocalTime startTime = interval.getDateStart();
                LocalTime endTime = interval.getDateEnd();

                // Filter available slots based on existing appointments
                List<AppointmentDto> appointments = appointmentService
                        .getAllAppointment(eventEntity.getUser());
                List<LocalTime> occupiedSlots = appointments.stream()
                        .filter(appointment -> isOverlapping(appointment, startTime, endTime))
                        .map(appointment -> appointment.getAppointmentStart().toLocalTime())
                        .collect(Collectors.toList());
                LocalDateTime combinedDateTime = LocalDateTime.of(specificDate, startTime);

                availableSlots.addAll(getAvailableSlots(combinedDateTime,startTime, endTime, duration, occupiedSlots));
            }
        }else{
            throw new EventException(MessageConstants.INTERVAL_EMPTY);
        }
        CalendarSlotsDto calendarSlotsDto = new CalendarSlotsDto();
        calendarSlotsDto.setName(weekDay.getName());
        calendarSlotsDto.setHours(availableSlots);

        return calendarSlotsDto;
    }

    // Método para encontrar el WeekDay por su DayOfWeek
    private WeekDay findWeekDayByDayOfWeek(List<WeekDay> weekDays, DayOfWeek dayOfWeek) {
        return weekDays.stream()
                .filter(weekDay -> Utilities.getDayOfWeek(weekDay.getName()) == dayOfWeek.getValue())
                .findFirst()
                .orElse(null);
    }

    private int calculateSlots(LocalTime startTime, LocalTime endTime, short duration) {
        int minutesBetween = (int) java.time.Duration.between(startTime, endTime).toMinutes();
        return minutesBetween / duration;
    }

    private List<LocalTime> getAvailableSlots(LocalDateTime specificDateTime, LocalTime startTime,
                                              LocalTime endTime, short duration,
                                              List<LocalTime> occupiedSlots) {
        List<LocalTime> availableSlots = new ArrayList<>();
        LocalDate today = LocalDate.now();

        int slots = calculateSlots(startTime, endTime, duration);
        for (long i = 0; i < slots; i++) {
            LocalTime slotTime = startTime.plusMinutes(i * duration);
            LocalDateTime slotDateTime = today.atTime(slotTime);
            LocalDateTime currentDateTime = LocalDateTime.now();

            // Verificar si specificDateTime es la fecha de hoy
            if (specificDateTime.toLocalDate().isEqual(today)) {
                // Agregar el slot si no está ocupado y no es una hora pasada
                if (!occupiedSlots.contains(slotTime) && slotDateTime.isAfter(currentDateTime)) {
                    availableSlots.add(slotTime);
                }
            } else {
                // Agregar el slot si no está ocupado
                if (!occupiedSlots.contains(slotTime)) {
                    availableSlots.add(slotTime);
                }
            }
        }

        return availableSlots;
    }

    private boolean isOverlapping(AppointmentDto appointment, LocalTime intervalStart, LocalTime intervalEnd) {
        LocalDateTime appointmentStart = appointment.getAppointmentStart();
        LocalDateTime appointmentEnd = appointment.getAppointmentEnd();

        // Convertir la fecha de la cita a un intervalo de tiempo dentro del día
        LocalTime appointmentStartTime = appointmentStart.toLocalTime();
        LocalTime appointmentEndTime = appointmentEnd.toLocalTime();

        return (appointmentStartTime.isBefore(intervalEnd) || appointmentStartTime.equals(intervalEnd))
                && (appointmentEndTime.isAfter(intervalStart) || appointmentEndTime.equals(intervalStart));
    }
}
