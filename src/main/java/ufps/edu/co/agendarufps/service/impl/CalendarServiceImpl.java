package ufps.edu.co.agendarufps.service.impl;


import com.google.api.client.googleapis.util.Utils;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.*;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.GoogleCredentials;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.config.CalendarConfig;
import ufps.edu.co.agendarufps.dtos.AppointmentDto;
import ufps.edu.co.agendarufps.dtos.EventDto;
import ufps.edu.co.agendarufps.dtos.UserDto;
import ufps.edu.co.agendarufps.entity.Appointment;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.mapping.EventMapping;
import ufps.edu.co.agendarufps.repository.IEventRepository;
import ufps.edu.co.agendarufps.service.ICalendarService;
import ufps.edu.co.agendarufps.service.exception.AppointmentException;
import ufps.edu.co.agendarufps.utils.MessageConstants;
import ufps.edu.co.agendarufps.utils.Utilities;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CalendarServiceImpl implements ICalendarService {
    private final CalendarConfig calendarConfig;
    private Calendar calendarInstance;
    private final IEventRepository eventService;

    private final String NAME_CALENDAR = "primary";
    private final String MEET_LOCATION = "Meet";
    private final String MIX_LOCATION = "Mixto";


    @Override
    public Calendar getCalendar(String token)  {

        HttpTransport httpTransport = Utils.getDefaultTransport();
        GsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
        try {
            if(calendarInstance == null) {
                GoogleCredentials credentials = GoogleCredentials.create(new AccessToken(token, null));
                calendarInstance = new Calendar.Builder(httpTransport, JSON_FACTORY, new HttpCredentialsAdapter(credentials))
                        .setApplicationName(calendarConfig.getApplicationName())
                        .build();
            }
        }catch (Exception e){
            System.err.println(MessageConstants.ERROR_GOOGLE_CALENDAR_INSTANCE);
            return null;
        }
        return calendarInstance;
    }

    @Override
        public Event insertEventAppointment( Appointment appointment)  {
        try{
            if(calendarInstance == null){
                getCalendar(new Utilities().getTokenFromAuth());
            }
            EventDto eventDto = EventMapping.eventEntityToEventDto(
                    eventService.findById(appointment.getEventEntity().getId()).get()
            );
            Event event = buildEvent(eventDto, appointment);
            if (((eventDto.getTypeMeeting().equalsIgnoreCase(MEET_LOCATION)
                    || eventDto.getTypeMeeting().equalsIgnoreCase(MIX_LOCATION))
                    && (appointment.getLocation().equalsIgnoreCase(MEET_LOCATION)))){
                return calendarInstance.events().insert(NAME_CALENDAR, event).setConferenceDataVersion(1).execute();
            }
            return calendarInstance.events().insert(NAME_CALENDAR, event).execute();
        } catch (IOException e){
            throw new AppointmentException(MessageConstants.GOOGLE_CALENDAR_ERROR);
        }
    }

    @Override
    public Event getEventGoogle(String eventId) throws IOException {
        if(calendarInstance == null){
            getCalendar(new Utilities().getTokenFromAuth());
        }
        try {
            return calendarInstance.events().get(NAME_CALENDAR, eventId).execute();
        }catch (IOException e){
            throw new IOException(e.getMessage());
        }
    }

    private Event buildEvent(EventDto eventDto, Appointment appointment){
        Event event = new Event();
        EventDateTime[] startAndEndEventDate = getEventStart(appointment.getAppointmentStart(), appointment.getEventEntity().getDuration());
        UserDto userOrganizer = eventDto.getUser();
        Event.Organizer organizer = new Event.Organizer();
        organizer.setEmail(userOrganizer.getEmail());

        event.setLocation(appointment.getLocation());
        event.setOrganizer(organizer);
        event.setDescription(buildDescription(eventDto, appointment));
        event.setAttendees(getAttenders(appointment.getInvitedUsers()));
        if(((eventDto.getTypeMeeting().equalsIgnoreCase(MEET_LOCATION)
                || eventDto.getTypeMeeting().equalsIgnoreCase(MIX_LOCATION))
                && (appointment.getLocation().equalsIgnoreCase(MEET_LOCATION)))){
            event.setConferenceData(buildConference());
        }
        event.setSummary(new Utilities().buildNameAppointment(appointment.getTopic().getName(), eventDto));
        event.setStart(startAndEndEventDate[0]);
        event.setEnd(startAndEndEventDate[1]);
        event.setColorId("11"); //red color event
        event.setEtag("Asesoria");
        event.setSource(buildSource());

        return event;
    }

    private ConferenceData buildConference(){
        ConferenceSolutionKey solutionKey = new ConferenceSolutionKey();
        solutionKey.setType("hangoutsMeet");

        CreateConferenceRequest createConferenceRequest = new CreateConferenceRequest();
        createConferenceRequest.setRequestId(UUID.randomUUID().toString());
        createConferenceRequest.setConferenceSolutionKey(solutionKey);

        ConferenceData conferenceData = new ConferenceData();
        conferenceData.setCreateRequest(createConferenceRequest);
        return conferenceData;
    }

    private List<EventAttendee> getAttenders(List<User> inviteUsers){
        List<EventAttendee> attenders = new ArrayList<>();
        if(!inviteUsers.isEmpty()) {
            EventAttendee attendee;
            for (User user : inviteUsers){
                attendee = new EventAttendee().setEmail(user.getEmail());
                attenders.add(attendee);
            }
        }
        return attenders;
    }

    private EventDateTime[] getEventStart(LocalDateTime date, short duration){
        EventDateTime[] eventDateTime = new EventDateTime[2];
        LocalDateTime start = date;
        LocalDateTime end = date.plusMinutes(duration);
        EventDateTime temp1 = new EventDateTime();
        EventDateTime temp2 = new EventDateTime();
        temp1.setTimeZone(calendarConfig.getTimeZone());
        temp2.setTimeZone(calendarConfig.getTimeZone());
        DateTime startDateTime = new DateTime(new Utilities().localDateTimeToDateTimeRFC3339(start));
        temp1.setDateTime(startDateTime);
        eventDateTime[0] = temp1;

        DateTime endDateTime = new DateTime(new Utilities().localDateTimeToDateTimeRFC3339(end));
        temp2.setDateTime(endDateTime);
        eventDateTime[1] = temp2;
        return eventDateTime;
    }
    private String buildDescription(EventDto eventDto, Appointment appointment) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Tema solicitado:\n");
        stringBuilder.append("- ").append(appointment.getTopic().getName());
        stringBuilder.append("\n\nComentarios:\n").append(appointment.getComments());
        stringBuilder.append("\n\nDescripción:\n").append(eventDto.getDescription());
        return stringBuilder.toString();
    }

    private Event.Source buildSource(){
        Event.Source source = new Event.Source();
        source.setTitle("Agendar UFPS");
        source.setUrl("https://agendar.ufps.edu.co");
        return source;
    }
}
