package ufps.edu.co.agendarufps.service.exception;

public class EventException extends RuntimeException{

    public EventException(String message) {
        super(message);
    }
    public static class EventNotFoundException extends RuntimeException {
        public EventNotFoundException(String message) {
            super(message);
        }
    }

    public static class EventAlreadyExistsException extends RuntimeException {
        public EventAlreadyExistsException(String message) {
            super(message);
        }
    }


}
