package ufps.edu.co.agendarufps.service;

import org.springframework.stereotype.Service;
import ufps.edu.co.agendarufps.dtos.AvailabilityDto;
import ufps.edu.co.agendarufps.entity.Availability;

import java.util.List;

@Service
public interface IAvailabilityService {
    AvailabilityDto saveAvailability(AvailabilityDto availability, String email);
    Availability getAvailability(Long idAvailability);
    AvailabilityDto updateAvailability(AvailabilityDto availability);
    void deleteAvailability(Long idAvailability);
    List<AvailabilityDto> getAllAvailability(String email);


}
