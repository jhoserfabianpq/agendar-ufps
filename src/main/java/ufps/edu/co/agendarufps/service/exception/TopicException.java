package ufps.edu.co.agendarufps.service.exception;

public class TopicException extends RuntimeException{
    public TopicException(String message) {
        super(message);
    }
    public static class TopicNotFoundException extends RuntimeException {
        public TopicNotFoundException(String message) {
            super(message);
        }
    }
    public static class TopicInUseException extends RuntimeException {
        public TopicInUseException(String message) {
            super(message);
        }
    }


}
