package ufps.edu.co.agendarufps.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ufps.edu.co.agendarufps.service.impl.EventServiceImpl;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class ScheduledTasks {

    private final EventServiceImpl eventService;

    // Llama al método para ejecutar el procedimiento almacenado el 30 de junio y el 30 de diciembre a las 12:00 AM
    //Segundos,Minutos, Horas, Día, Mes, Día de la semana
    @Scheduled(cron = "0 0 0 30 6,12 ?", zone = "America/Bogota")
    public void executeStoredProcedure() {
        eventService.procedureCleanEventPeriod();
    }
}
