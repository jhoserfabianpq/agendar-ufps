package ufps.edu.co.agendarufps.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.stereotype.Component;
import ufps.edu.co.agendarufps.dtos.EventDto;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.TreeMap;

@Component
public class Utilities {

    private static final int[] FIRST_PERIOD_MONTH_DAY  = {6,30};
    private static final int[] SECOND_PERIOD_MONTH_DAY  = {12,30};

    public static int getDayOfWeek(String day) {
        Map<String, Integer> mapaDiasSemana = new TreeMap<>();
        mapaDiasSemana.put( "Lunes", 1 );
        mapaDiasSemana.put( "Martes", 2 );
        mapaDiasSemana.put( "Miercoles", 3 );
        mapaDiasSemana.put( "Jueves", 4);
        mapaDiasSemana.put( "Viernes", 5);
        mapaDiasSemana.put( "Sabado", 6 );
        mapaDiasSemana.put( "Domingo", 7 );
        return mapaDiasSemana.get(day);
    }

    public static String getPeriod(LocalDateTime date){
        int year = date.getYear();
        int month = date.getMonthValue();
        int day = date.getDayOfMonth();
        // Si es menor o igual al 30 de junio, usar el periodo siguiente año más el mes 01
        if ((month < FIRST_PERIOD_MONTH_DAY[0]) || (month == FIRST_PERIOD_MONTH_DAY[0]
                && day <= FIRST_PERIOD_MONTH_DAY[1])) {
            return year + "01";
        } else if (month == SECOND_PERIOD_MONTH_DAY [0] && day > SECOND_PERIOD_MONTH_DAY [1]) {
            // Si es después del 30 de diciembre, usar el periodo siguiente año más el mes 01
            return (year + 1) + "01";
        } else {
            // Si no cumple ninguna de las condiciones anteriores, usar el periodo actual año más el mes 02
            return year + "02";
        }
    }


    public String getEmailAddressWithoutDomain(String email){
        return email.split("@")[0];
    }

    public String buildNameAppointment(String topicName, EventDto event){
        return event.getName() + " - " + topicName;
    }

    public long localDateTimeToDateTimeRFC3339(LocalDateTime date){
        ZoneId zoneId = ZoneId.systemDefault();
        return date.atZone(zoneId).toInstant().getEpochSecond() * 1000;
    }

    public String getTokenFromAuth(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // Verifica si la autenticación es de tipo OAuth2IntrospectionAuthenticatedPrincipal
        if (authentication != null && authentication.getPrincipal() instanceof OAuth2IntrospectionAuthenticatedPrincipal user) {
            // Ahora 'user' contiene la información del usuario autenticado
            return user.getAttribute("token");
        }
        return null;
    }
}
