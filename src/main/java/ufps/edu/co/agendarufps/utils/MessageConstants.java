package ufps.edu.co.agendarufps.utils;

public class MessageConstants {

    /** EVENTOS **/
    public static final String EVENT_NOT_FOUND = "Evento no encontrado.";
    public static final String EVENT_ALREADY_EXISTS = "El evento ya existe.";
    public static final String EVENT_NAME_IS_REQUIRED = "Se requiere un nombre para el evento.";
    public static final String EVENT_ERROR = "Se produjo un error al procesar el evento.";
    public static final String INTERVAL_EMPTY = "El intervalo está vacío. Por favor, seleccione un día de la semana válido.";
    public static final String EVENT_DELETE = "Evento eliminado correctamente.";

    /** USUARIO **/
    public static final String UNAUTHORIZED = "El usuario no está autorizado.";
    public static final String USER_NOT_FOUND = "Usuario no encontrado.";
    public static final String REPORT_ERROR = "Error al generar el informe Excel.";
    public static final String REPORT_ERROR_HISTORIC = "Error al acceder a los campos de la entidad Histórico.";

    /** TEMA **/
    public static final String TOPIC_NOT_FOUND = "Tema no encontrado.";
    public static final String TOPIC_IN_USE = "Error al eliminar el tema, está en uso en un evento.";
    public static final String TOPIC_NAME_IS_REQUIRED = "Se requiere un nombre para el tema.";
    public static final String TOPIC_ERROR = "Se produjo un error al procesar el tema.";
    public static final String TOPIC_DELETE = "Tema eliminado correctamente.";

    /** DISPONIBILIDAD **/
    public static final String AVAILABILITY_NOT_FOUND = "Disponibilidad no encontrada.";
    public static final String ERROR_GOOGLE_CALENDAR_INSTANCE = "Error al crear la instancia de Google Calendar.";
    public static final String EMPTY_FIELDS = "Los campos son requeridos y no deben dejarse en blanco.";
    public static final String AVAILABILITY_DELETE = "Disponibilidad eliminada correctamente.";
    public static final String AVAILABILITY_IN_USE = "Error al eliminar el tema, está en uso en un evento.";
    public static final String AVAILABILITY_ERROR = "Se produjo un error al procesar la disponibilidad.";

    /** CITA **/
    public static final String GOOGLE_CALENDAR_ERROR = "Se produjo un error al procesar la cita en Google Calendar.";
    public static final String EMPTY_APPOINTMENT_UPCOMING = "No tienes citas pendientes.";
    public static final String EMPTY_APPOINTMENT = "No tienes citas pendientes.";
    public static final String APPOINTMENT_NOT_FOUND = "Cita no encontrada.";
    public static final String APPOINTMENT_MARK_ATTENDED = "Se ha cambiado la asistencia del asistente.";
}
