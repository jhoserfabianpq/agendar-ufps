package ufps.edu.co.agendarufps.entity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "appointment")
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_appointment")
    private Integer id;
    @Column(name = "name")
    private String name;

    @Column(name = "period")
    private String period;

    @Column(name = "appointment_start")
    private LocalDateTime appointmentStart;

    @Column(name = "url_meet", length = 45)
    private String urlMeet;

    @Column(name = "location")
    private String location;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "event_id", nullable = false)
    @JsonBackReference
    private EventEntity eventEntity;

    @ManyToMany()
    @JoinTable(
            name = "user_appointment",
            joinColumns = @JoinColumn(name = "id_appointment"),
            inverseJoinColumns = @JoinColumn(name = "id_user")
    )
    @JsonBackReference
    private List<User> invitedUsers = new ArrayList<>();
    @OneToMany(mappedBy = "appointment")
    private List<UserAppointment> userAppointments = new ArrayList<>();

    @Column(name = "appointment_end")
    private LocalDateTime appointmentEnd;


    @Column(name = "comments")
    private String comments;

    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "id_google_calendar")
    private String idGoogleCalendar;

    @Column(name = "link_google_calendar")
    private String linkGoogleCalendar;

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", appointmentStart=" + appointmentStart +
                ", urlMeet='" + urlMeet + '\'' +
                ", eventEntity=" + eventEntity +
                ", invitedUsers=" + invitedUsers +
                ", appointmentEnd=" + appointmentEnd +
                ", createdAt=" + createdAt +
                '}';
    }
}
