package ufps.edu.co.agendarufps.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@Embeddable
public class UserAppointmentId implements Serializable {

    @Column(name = "id_user")
    private Long userId;

    @Column(name = "id_appointment")
    private Integer appointmentId;

}
