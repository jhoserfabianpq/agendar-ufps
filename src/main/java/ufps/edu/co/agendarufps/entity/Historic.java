package ufps.edu.co.agendarufps.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "historic")
public class Historic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_historic")
    private Integer id;

    @Column(name = "email_organizer")
    private String emailOrganizer;

    @Column(name = "event_name")
    private String eventName;

    @Column(name = "description")
    private String description;

    @Column(name = "topics")
    private String topics;

    @Column(name = "attendees")
    private String attendees;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "duration")
    private Short duration;

    @Column(name = "date_end")
    private LocalDateTime dateEnd;

    @Column(name = "period")
    private String period;

    @Column(name = "date_start")
    private LocalDateTime dateStart;
}
