package ufps.edu.co.agendarufps.entity;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@Entity
@Table(name = "user_appointment")
public class UserAppointment {

    @EmbeddedId
    private UserAppointmentId id;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "id_user")
    private User user;

    @ManyToOne
    @MapsId("appointmentId")
    @JoinColumn(name = "id_appointment")
    private Appointment appointment;

    @Column(name = "attended")
    private boolean attended;

}