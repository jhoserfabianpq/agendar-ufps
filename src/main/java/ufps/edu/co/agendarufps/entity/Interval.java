package ufps.edu.co.agendarufps.entity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "intervals")
public class Interval {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_intervals")
    private Integer id;

    @Column(name = "date_start")
    private LocalTime dateStart;

    @Column(name = "date_end")
    private LocalTime dateEnd;

    @ManyToOne
    @JoinColumn(name = "week_day_id", nullable = false)
    @JsonBackReference
    private WeekDay weekDay;

    // Getters and setters

    // Additional methods as needed
}