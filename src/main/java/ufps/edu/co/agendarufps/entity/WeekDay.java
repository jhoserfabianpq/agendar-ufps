package ufps.edu.co.agendarufps.entity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "week_day")
public class WeekDay {

    @Id
    @Column(name = "id_week")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    // cambiar a int que indique el dia de la semana.
    @Column(name = "name", nullable = false, length = 45)
    private String name;

    @ManyToOne
    @JoinColumn(name = "availability_id", nullable = false)
    @JsonBackReference
    private Availability availability;

    @Column(name = "state", nullable = false)
    private Byte state;

    @OneToMany(mappedBy = "weekDay", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    private List<Interval> intervals;

}