package ufps.edu.co.agendarufps.entity;

import com.fasterxml.jackson.annotation.*;
import jakarta.persistence.*;
import lombok.*;
import net.minidev.json.annotate.JsonIgnore;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "event")
public class EventEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_event")
    private Long id;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "name", nullable = false, length = 45)
    private String name;

    @Column(name = "duration", nullable = false)
    private short duration;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "type_meeting", nullable = false)
    private String typeMeeting;

    @Column(name = "state")
    private Byte state;


    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "period")
    private String period;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id_user", nullable = false)
    @JsonBackReference
    private User user;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "availability_id")
    private Availability availability;

    @JsonManagedReference
    @OneToMany(mappedBy = "eventEntity", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private List<Appointment> appointments;

    @ManyToMany
    @JoinTable(
            name = "topics_event",
            joinColumns = @JoinColumn(name = "event_id", referencedColumnName ="id_event"),
            inverseJoinColumns = @JoinColumn(name = "topics_id", referencedColumnName ="id_topic")
    )
    private List<Topic> topics;

}
