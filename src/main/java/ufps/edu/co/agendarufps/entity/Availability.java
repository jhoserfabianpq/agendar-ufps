package ufps.edu.co.agendarufps.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "availability")
public class Availability {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_availability")
    private Long id;

    @Column(name = "name", length = 45)
    private String name;

    @JsonBackReference
    @OneToMany(mappedBy = "availability", fetch = FetchType.EAGER)
    private List<EventEntity> eventEntity;

    @ManyToOne
    @JoinColumn(name = "created_by", referencedColumnName = "id_user")
    @JsonBackReference
    private User createdBy;
    @OneToMany(mappedBy = "availability", cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JsonManagedReference
    private List<WeekDay> weekDays;

}