package ufps.edu.co.agendarufps.dtos;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EventDto {

    @JsonProperty(value = "id_event")
    private Long id;

    private String description;
    private String name;

    @JsonIgnore
    private Boolean isDeleted;

    private short duration;
    private String location;
    private String typeMeeting;
    private Byte state;

    @JsonProperty(value = "created_at")
    private LocalDateTime createdAt;

    @JsonProperty(value = "updated_at")
    private LocalDateTime updatedAt;

    private Integer version;

    @JsonProperty(value = "user_id")
    @JsonBackReference("User-Event")
    private UserDto user;

    AvailabilityDto availability;

    /*@JsonManagedReference("Event-Appointment")
    private List<AppointmentDto> appointments;
*/
    @JsonIgnoreProperties("events")
    private List<TopicDto> topics;

    @Override
    public String toString() {
        return "EventDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", duration=" + duration +
                ", location='" + location + '\'' +
                ", state=" + state +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", version=" + version +
                ", user=" + user +
                ", availabilities=" + availability +
                ", topics=" + topics +
                '}';
    }
}
