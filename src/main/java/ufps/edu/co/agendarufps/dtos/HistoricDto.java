package ufps.edu.co.agendarufps.dtos;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HistoricDto {

    private String emailOrganizador;

    private String nombreEvento;

    private String descripcion;

    private String tema;

    private String asistentes;

    private LocalDateTime fechaCreacion;

    private Short duracion;

    private LocalDateTime fechaFin;

    private String periodo;

    private LocalDateTime fechaInicio;
}
