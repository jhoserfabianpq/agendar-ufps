package ufps.edu.co.agendarufps.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAppointmentDto {

    @JsonIgnore
    private UserAppointmentIdDto id;

    private UserDto user;


    private boolean attended;

}
