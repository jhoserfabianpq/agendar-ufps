package ufps.edu.co.agendarufps.dtos;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AvailabilityDto {
    @JsonProperty(value = "id_availability")
    private Long id;

    private String name;

    /*@JsonProperty(value = "event_id")
    @JsonManagedReference("Event-Availability")
    @JsonIgnoreProperties("topics")
    private List<Long> eventIds;*/

    private UserDto createdBy;


    @JsonManagedReference("Availability-WeekDay")
    private List<WeekDayDto> weekDays;

}
