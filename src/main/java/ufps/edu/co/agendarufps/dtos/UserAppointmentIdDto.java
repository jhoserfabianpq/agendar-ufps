package ufps.edu.co.agendarufps.dtos;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAppointmentIdDto {
    private Long userId;
    private Integer appointmentId;
}
