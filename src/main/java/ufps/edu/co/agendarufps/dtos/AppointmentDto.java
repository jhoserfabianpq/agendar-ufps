package ufps.edu.co.agendarufps.dtos;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.JoinColumn;
import lombok.*;
import ufps.edu.co.agendarufps.entity.UserAppointment;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class AppointmentDto {
    @JsonProperty(value = "id_appointment")
    private Integer id;

    private String name;

    @JsonProperty(value = "appointment_start")
    private LocalDateTime appointmentStart;

    private String location;

    @JsonProperty(value = "url_meet")
    private String urlMeet;

    @JsonProperty(value = "event_id")
    @JsonIgnoreProperties({"availability", "topics"})
    private EventDto eventEntity;

    private String comments;

    private TopicDto topic;

    @JsonProperty(value = "user_appointment")
    private List<UserDto> invitedUsers = new ArrayList<>();

    @JsonProperty(value = "appointment_end")
    private LocalDateTime appointmentEnd;

    @JsonProperty(value = "created_at")
    private LocalDateTime createdAt;

    @JsonProperty(value = "id_google_calendar")
    private String idGoogleCalendar;

    @JsonProperty(value = "link_google_calendar")
    private String linkGoogleCalendar;

    @JsonIgnore
    private List<UserAppointmentDto> userAppointments = new ArrayList<>();
}
