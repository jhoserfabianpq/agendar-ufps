package ufps.edu.co.agendarufps.dtos;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WeekDayDto {

    @JsonProperty(value = "id_week")
    private Integer id;

    private String name;

    @JsonProperty(value = "availability_id")
    @JsonBackReference("Availability-WeekDay")
    private AvailabilityDto availability;

    private Byte state;
    @JsonManagedReference("WeekDay-Interval")
    private List<IntervalDto> intervals;
}
