package ufps.edu.co.agendarufps.dtos;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IntervalDto {

    @JsonProperty(value = "id_intervals")
    private Integer id;

    @JsonProperty(value = "date_start")
    private LocalTime dateStart;

    @JsonProperty(value = "date_end")
    private LocalTime dateEnd;

    @JsonProperty(value = "week_day_id")
    @JsonBackReference("WeekDay-Interval")
    private WeekDayDto weekDay;
}

