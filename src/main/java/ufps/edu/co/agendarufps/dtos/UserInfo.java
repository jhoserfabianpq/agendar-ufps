package ufps.edu.co.agendarufps.dtos;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserInfo{
        String sub;
        String name;
        String given_name;
        String family_name;
        String picture;
        String email;
        boolean email_verified;
        String locale;
}
