package ufps.edu.co.agendarufps.dtos;

public record UrlDto (String authURL) { }
