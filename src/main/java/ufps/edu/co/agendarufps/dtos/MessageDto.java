package ufps.edu.co.agendarufps.dtos;

public record MessageDto(String message) {
}
