package ufps.edu.co.agendarufps.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TopicDto {
    @JsonProperty(value = "id_topic")
    private Integer id;

    private String name;
    @JsonIgnore
    private Boolean isDeleted;

    @JsonProperty(value = "created_at")
    private LocalDateTime createdAt;


    @JsonProperty(value = "updated_at")
    private LocalDateTime updatedAt;


    private Integer version;

    @JsonProperty(value = "created_by")
    private String emailCreatedBy;
}
