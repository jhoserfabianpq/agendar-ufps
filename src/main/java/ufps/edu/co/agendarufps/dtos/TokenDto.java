package ufps.edu.co.agendarufps.dtos;

public record TokenDto(String token) {
}
