package ufps.edu.co.agendarufps.dtos;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GeneralReportDto {
    String eventName;
    String topicName;
    int countAppointmentXTopic;
    int virtualAppointments;
    int inPersonAppointments;
    int totalAttendedUsers;
    int totalMissedUsers;
    int averageTime;

}
