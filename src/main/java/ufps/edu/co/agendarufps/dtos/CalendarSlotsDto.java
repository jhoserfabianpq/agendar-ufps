package ufps.edu.co.agendarufps.dtos;

import lombok.*;

import java.time.LocalTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CalendarSlotsDto {
    String name;
    List<LocalTime> hours;
}
