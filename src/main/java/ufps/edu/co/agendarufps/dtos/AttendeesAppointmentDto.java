package ufps.edu.co.agendarufps.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AttendeesAppointmentDto {

    @JsonIgnoreProperties("user_appointment")
    private AppointmentDto appointment;
    @JsonProperty("attendees")
    public List<UserAppointmentDto> getAttendees() {
        return appointment.getUserAppointments();
    }
}
