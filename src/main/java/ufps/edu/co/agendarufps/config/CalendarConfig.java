package ufps.edu.co.agendarufps.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Getter
@RequiredArgsConstructor
public class CalendarConfig {

    @Value("${spring.security.oauth2.resourceserver.opaque-token.clientId}")
    private String googleClientId;

    @Value("${spring.security.oauth2.resourceserver.opaque-token.clientSecret}")
    private String googleClientSecret;

    @Value("${spring.application.google.timeZone}")
    private String timeZone;

    @Value("${spring.application.google.applicationName}")
    private String applicationName;

    @Value("${spring.application.google.redirect-uri}")
    private String redirectUri;

    @Value("${spring.application.google.scope}")
    private List<String> scopes;

}