package ufps.edu.co.agendarufps.config;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;
import ufps.edu.co.agendarufps.dtos.UserInfo;
import ufps.edu.co.agendarufps.entity.User;
import ufps.edu.co.agendarufps.mapping.UserMapping;
import ufps.edu.co.agendarufps.utils.MessageConstants;
import ufps.edu.co.agendarufps.utils.Utilities;

import java.util.HashMap;
import java.util.Map;



@RequiredArgsConstructor
public class GoogleOpaqueTokenIntrospector implements OpaqueTokenIntrospector {

    private final WebClient userInfoClient;


    @Override
    public OAuth2AuthenticatedPrincipal introspect(String token) {
        try{
            UserInfo userInfo = userInfoClient.get()
                    .uri( uriBuilder -> uriBuilder
                            .path("/oauth2/v3/userinfo")
                            .queryParam("access_token", token)
                            .build())
                    .retrieve()
                    .onStatus(httpStatus -> httpStatus == HttpStatus.UNAUTHORIZED,
                            response -> handleUnauthorized(response).createException())
                    .bodyToMono(UserInfo.class)
                    .block();

            Map<String, Object> attributes = new HashMap<>();
            User user = new UserMapping().UserInfoToUser(userInfo);
            attributes.put("first_name", user.getFirstName());
            attributes.put("last_name", user.getLastName());
            attributes.put("picture_url", user.getPictureUrl());
            attributes.put("email", user.getEmail());
            attributes.put("created_at", user.getCreatedAt());
            attributes.put("updated_at", user.getUpdatedAt());
            attributes.put("nickname", new Utilities().getEmailAddressWithoutDomain(user.getEmail()));
            attributes.put("token", token);
            return new OAuth2IntrospectionAuthenticatedPrincipal( attributes, null);
        } catch (WebClientResponseException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    private ClientResponse handleUnauthorized(ClientResponse response) {
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, MessageConstants.UNAUTHORIZED);
    }

}
