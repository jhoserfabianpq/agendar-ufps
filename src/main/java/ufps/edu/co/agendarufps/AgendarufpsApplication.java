package ufps.edu.co.agendarufps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgendarufpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgendarufpsApplication.class, args);
	}

}
