package ufps.edu.co.agendarufps.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ufps.edu.co.agendarufps.entity.Availability;

import java.util.List;

public interface IAvailabilityRepository extends JpaRepository<Availability, Long> {
    List<Availability> findAllByCreatedByEmail(String email);
}
