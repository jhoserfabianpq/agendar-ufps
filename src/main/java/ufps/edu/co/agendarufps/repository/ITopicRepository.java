package ufps.edu.co.agendarufps.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ufps.edu.co.agendarufps.entity.Topic;
import ufps.edu.co.agendarufps.entity.User;

import java.util.List;

public interface ITopicRepository extends JpaRepository<Topic,Integer> {
    Topic findByIdAndCreatedBy(Integer id, User createdBy);
    List<Topic> findAllByCreatedByEmail(String email);
}
