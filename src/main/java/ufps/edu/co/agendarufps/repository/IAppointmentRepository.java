package ufps.edu.co.agendarufps.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ufps.edu.co.agendarufps.entity.Appointment;
import ufps.edu.co.agendarufps.entity.User;

import java.time.LocalDateTime;
import java.util.List;

public interface IAppointmentRepository extends JpaRepository<Appointment,Integer> {
    List<Appointment> findByInvitedUsersEmailOrderByAppointmentStartAsc(String email);
    List<Appointment> findByInvitedUsersEmailAndAppointmentStartAfterOrderByAppointmentStartAsc(String invitedUsers_email
            , LocalDateTime appointmentStart);
    List<Appointment> findByEventEntityUserEmailOrderByAppointmentStartAsc(String email);

    /* REPORT */
    List<Appointment> findAllByTopicIdAndEventEntityIdAndPeriod(Integer idTopic, Long idEvent, String period);
    List<Appointment> findAllByInvitedUsersEmailAndPeriod(String email, String period);


}
