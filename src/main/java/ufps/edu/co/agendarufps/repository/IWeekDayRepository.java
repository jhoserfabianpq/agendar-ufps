package ufps.edu.co.agendarufps.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ufps.edu.co.agendarufps.entity.WeekDay;

public interface IWeekDayRepository extends JpaRepository<WeekDay, Integer> {
    WeekDay findByNameAndAvailabilityId(String name, long idAvailability);
    void deleteByAvailabilityId(long idAvailability);
}
