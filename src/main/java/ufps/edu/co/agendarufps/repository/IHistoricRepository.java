package ufps.edu.co.agendarufps.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ufps.edu.co.agendarufps.entity.Historic;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface IHistoricRepository extends JpaRepository<Historic, Integer> {
    List<Historic> findHistoricByEmailOrganizer(String email);
    List<Historic> findHistoricByEmailOrganizerAndPeriod(String email, String period);
    List<Historic> findHistoricByDateStartBetween(LocalDateTime start, LocalDateTime end);
}
