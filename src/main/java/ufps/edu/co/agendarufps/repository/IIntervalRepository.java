package ufps.edu.co.agendarufps.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ufps.edu.co.agendarufps.entity.Interval;

public interface IIntervalRepository extends JpaRepository<Interval, Integer> {
}
