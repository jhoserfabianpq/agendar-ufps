package ufps.edu.co.agendarufps.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ufps.edu.co.agendarufps.entity.UserAppointment;
import ufps.edu.co.agendarufps.entity.UserAppointmentId;

import java.util.List;

public interface IUserAppointmentRepository extends JpaRepository<UserAppointment, UserAppointmentId> {
    List<UserAppointment> findAllByAppointmentId(Integer appointmentId);
}
