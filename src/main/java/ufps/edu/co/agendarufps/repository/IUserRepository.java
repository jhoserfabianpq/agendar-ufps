package ufps.edu.co.agendarufps.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ufps.edu.co.agendarufps.entity.User;


public interface IUserRepository extends JpaRepository<User, Integer> {

    User findByEmail(String email);
    @Query("SELECT u FROM User u WHERE SUBSTRING_INDEX(u.email, '@', 1) = :username")
    User findByUsernameWithoutDomain(@Param("username") String username);
}
