package ufps.edu.co.agendarufps.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import ufps.edu.co.agendarufps.entity.EventEntity;

import java.util.List;
import java.util.Optional;

public interface IEventRepository extends JpaRepository<EventEntity, Long> {
    Optional<EventEntity> findByNameAndUserEmail(String name, String userEmail);
    List<EventEntity> findAllByUserEmail(String userEmail);
    List<EventEntity> findAllByUserEmailAndIsDeleted(String userEmail, Boolean isDeleted);
    List<EventEntity> findAllByUserEmailAndIsDeletedAndPeriod(String userEmail, Boolean isDeleted, String period);

    @Procedure( procedureName = "updateEvents")
    void procedureCleanEvent();
}
