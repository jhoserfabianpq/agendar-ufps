<div align="center">
  <img src="https://gitlab.com/ronaldeduardobm/agendarufps/-/raw/main/src/assets/images/logo-white.svg" alt="Logo AGENDAR">
</div>

## Descripción
El proyecto propone desarrollar una plataforma accesible y fácil de usar que integre las cuentas institucionales de Google con el servicio de Google Calendar para facilitar la programación y gestión de asesorías entre profesores y estudiantes en la Universidad Francisco de Paula Santander (UFPS). Este sistema permitirá automatizar la reserva de citas, reduciendo así los errores humanos y las ineficiencias asociadas con los métodos manuales.

## Tecnologías
* Spring Boot 3.2.1
* Java 17 o superior
* Maven 4.0.0
* MYSQL


### Requisitos previos
* Tecnologías anteriormente mencionadas
* IDE Intelij
* Proyecto en Google Cloud Platform
    * El proyecto tiene que tener habilitado el servicio de 'Google Calendar API', con este servicio habilitado creamos unas credenciales de tipo 'ID de clientes OAuth 2.0'. Donde debemos colocar los Origines autorizados de Javascript y el URI de redireccionamiento autorizados.
    * En la pantalla de consentiemiento tenemos que habilitar los siguientes permisos
        * auth/userinfo.email
        * auth/userinfo.profile
        * openid
        * auth/calendar

* Configurar las siguientes variables de entorno en un archivo llamado 'secrets-prod.properties' ubicado en la carpeta resources/
```
DB_USER=usuario de base de datos
DB_PASSWORD=contraseña de la base de datos
DB_URL=Url de la base de datos

FRONTEND_HOST=URL Front por cors 

GOOGLE_CLIENT_ID=Id del cliente generado por las credenciales de google
GOOGLE_CLIENT_SECRET=Secreto del cliente generado por las credenciales de google
GOOGLE_CLIENT_NAME=Nombre del proyecto de google
GOOGLE_REDIRECT_URI=URI de redireccionamiento creado en las credenciales de google
```
## Estructura del proyecto
```bash
│ 
├─ src/
│   │
│   └─ main/
│       ├─ java.ufps.edu.co.agendarufps/
│       │   ├─ config/
│       │   ├─ controller/
│       │   ├─ dtos/
│       │   ├─ mapping/
│       │   ├─ repository/
│       │   ├─ service/
│       │   └─ utils/
│       │
│       └─ resources/
│
└─ pom.xml
```
**config**: Contiene la configuración necesaria para el proyecto, como configuración de Google, configuración de seguridad, etc.

**controller**: Contiene todos los controladores de la API REST, que manejan las solicitudes HTTP y las respuestas.

**dtos**: Contiene los Data Transfer Objects (DTOs), que se utilizan para transferir datos entre el cliente y el servidor.

**mapping**: Contiene clases para mapear entre entidades y DTOs, como mappers o convertidores.

**repository**: Contiene las interfaces de los repositorios JPA, que definen métodos para acceder a la base de datos.

**service**: Contiene la lógica de negocio de la aplicación, implementando la lógica de procesamiento de datos.

**utils**: Contiene utilidades y clases de ayuda que se utilizan en toda la aplicación.

**resources**: Contiene archivos de recursos como archivos de configuración, archivos de propiedades y archivos yml.

**pom.xml**: Archivo de configuración de Maven que define las dependencias y la configuración del proyecto.

## Ejecución del proyecto
```
git clone https://gitlab.com/jhoserfabianpq/agendar-ufps.git
cd agendar-ufps
 ```
Abrir con el IDE de preferencia, se recomienda Intellij ya que detectarás las dependencias y las instalará automáticamente para mayor facilidad.
Ejecutamos y tendremos el proyecto funcionando

## Endpoint
Puedes acceder a la documentación completa de la API y probar los endpoints utilizando Swagger en:
 ```
 http://127.0.0.1:8080/agendarbackend/swagger-ui.html
  ```



## Desarrollado Por:

- **Jhoser Fabian Pacheco Quintero** - Código: 1151807
    - **Correo**: [jhoserfabianpq@ufps.edu.co](mailto:jhoserfabianpq@ufps.edu.co)

- **Ronald Eduardo Benitez Mejia** - Código: 1151813
    - **Correo**: [ronaldeduardobm@ufps.edu.co](mailto:ronaldeduardobm@ufps.edu.co)

## Dirección del Proyecto

- **Director**: PhD. Marco Antonio Adarme Jaimes
- **Co-Directora**: PhD. Judith del Pilar Rodríguez Tenjo


___

<div style="display: flex;">
  <img src="https://gitlab.com/ronaldeduardobm/agendarufps/-/raw/main/src/assets/images/ufps.png" alt="Logos institucionales">
</div> 

